﻿Prepare envirment:
	Install Python3 packages:
		sudo apt-get install python3 python3-pip python3-virtualenv

	Install tensorflow and other packages for pip:
		for req in $(cat requirements.txt); do pip3 install $req; done

Note: if network speed is slow and the pip installation is affected, you can execute:
    for req in $(cat requirements.txt); do pip3 install $req -i https://pypi.tuna.tsinghua.edu.cn/simple; done

Conversion_scripts:
1、Convert Model to case code:
	you only need Configure corresponding parameters and execute:
		sh 0_import_model.sh
		sh 1_quantize_model.sh
		sh 2_export_case_code.sh


2、Get the mapping of the output
	python extractoutput.py   xxx.json

	


