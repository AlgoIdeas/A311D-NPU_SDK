import json
import sys
import os
import dill
#support build-in functions:
'''
    def build_port(self, ly_str, pt=0):
    def have_const_in_inputs(self, tensor):
    def have_single_tensor_in_inputs(self, tensor):
    def input_port_is_const(self, tensor, pt):
    def shape_pick(self, tensor):
    def attr_pick(self, tensor, key, default=0):
    def array_layout(self, array, layout):
    def tensor_to_numpy(self, tensor_name, trans=None):
    def squeeze_shapes(self, squeeze_dims, input_shape):
    def fc_weight(self, in_tensor, node):
    def split_slice_cale(self, slice):
    def map_pad_value(self, node):
'''

ruler_list = list()

def rule_pyfunc_def(func):
    def _wrap_func(*args, **kwargs):
        src = dill.source.getsource(func)
        src = src.replace('\r\n', '\n')
        src = src.replace('@rule_pyfunc_def\n', '')
        src = src.split('\n')
        return ['__rule_func_additional_args = ' + json.dumps(kwargs)] + src if len(kwargs) > 0 else src
    return _wrap_func

@rule_pyfunc_def
def r_softmax_get_sf_axis(self, node, tensor):
    axis = self.attr_pick(node['Softmax'], 'axis', None)
    if axis is None:
        shape = self.shape_pick(tensor['I:out0'])
        if len(shape) == 4:
            axis = 1
        else:
            axis = -1
    return axis

@rule_pyfunc_def
def r_softmax_get_log_sf_axis(self, node, tensor):
    axis = self.attr_pick(node['LogSoftmax'], 'axis', None)
    if axis is None:
        shape = self.shape_pick(tensor['I:out0'])
        if len(shape) == 4:
            axis = 1
        else:
            axis = -1
    return axis

@rule_pyfunc_def
def r_slice_get_size(self, node, tensor):
    starts = self.attr_pick(node['Slice'], 'starts', None)
    ends = self.attr_pick(node['Slice'], 'ends', None)
    axes = self.attr_pick(node['Slice'], 'axes', None)
    in_shape = self.shape_pick(tensor['I:out0'])
    out_shape = self.shape_pick(tensor['Slice:out0'])

    import numpy as np
    import copy
    INT_MAX = np.iinfo(np.int64).max
    in_shape = copy.deepcopy(in_shape)
    ends = copy.deepcopy(ends)
    for i in range(len(axes)):
        if ends[i] == INT_MAX:
            ends[i] = out_shape[axes[i]]
        in_shape[axes[i]] = ends[i] - starts[i]
    size = in_shape
    return size

@rule_pyfunc_def
def r_slice_get_begin(self, node, tensor):
    in_shape = self.shape_pick(tensor['I:out0'])
    starts = self.attr_pick(node['Slice'], 'starts', None)
    axes = self.attr_pick(node['Slice'], 'axes', None)
    begin = [0] * len(in_shape)
    for i in range(len(axes)):
        begin[axes[i]] = starts[i]
    return begin

@rule_pyfunc_def
def r_get_deconv_weights(self, node, tensor):
    in_channel = self.shape_pick(tensor['Constant_0:out0'])[1]
    group = self.attr_pick(node['ConvTranspose'], 'group', 1)
    weights = in_channel * group
    return weights

@rule_pyfunc_def
def r_group_conv1d_pre_condition(self, node, tensor):
    ret = False
    if len(self.shape_pick(tensor['Constant_0:out0'])) == 3:
        in_shape = self.shape_pick(tensor['I:out0'])
        group_number = self.attr_pick(node['Conv'], 'group', 1)
        if group_number > 1:
            ret = True
    return ret


@rule_pyfunc_def
def r_depthwise_conv1d_pre_condition(self, node, tensor):
    ret = False
    if len(self.shape_pick(tensor['Constant_0:out0'])) == 3:
        in_shape = self.shape_pick(tensor['I:out0'])
        group_number = self.attr_pick(node['Conv'], 'group', 1)
        if group_number > 1 and group_number == in_shape[1]:
            ret = True
    return ret

@rule_pyfunc_def
def r_pad_value_map(self, node, tensor):
    pad_np = self.tensor_to_numpy(tensor['Constant:out0'])
    pads = list(pad_np)
    pads = [int(p) for p in pads]
    dims = len(pads) // 2
    pads_array = list()
    if dims == 4:
        for id in range(dims):
            pad = [pads[id], pads[dims + id]]
            pads_array.append(pad)
    elif dims < 4:
        for id in range(dims):
            pad = [pads[id], pads[dims + id]]
            pads_array.append(pad)
    return pads_array


r_variable = {
"ruler_name": "r_variable",
"src_ops_alias": ["Constant"],
"src_inter_flow": [],
"src_in_anchor": [],
"src_out_tensor": ["Constant:out0"],
"acu_lys_alias": ["variable"],
"src_acu_in_tensor_map": [],
"src_acu_out_tensor_map": [["Constant:out0", "variable:out0"]],
"param_map": {"variable": {'shape': ['ORIGIN', 'CODE', "self.shape_pick(tensor['Constant:out0'])"]}},
"blob_map": {"variable": {'data':
                              ['CODE',
                               "np.array([self.tensor_to_numpy(tensor['Constant:out0'])]) "\
                               " if self.tensor_to_numpy(tensor['Constant:out0']).shape == () "\
                               "else self.tensor_to_numpy(tensor['Constant:out0'])"],}},
"acu_inter_flow": [],
"priority_tip": 0,
"pre_condition": None,
"src_ops_main_version": None,
"src_ops_minior_version": [1, -1]}
ruler_list.append(r_variable)

r_rsp_mm_add = {
"ruler_name": "r_rsp_mm_add",
"src_ops_alias": ["Reshape", "MatMul", "Add", "Constant_0", "Constant_1"],
"src_inter_flow":
    [["Reshape:out0", "MatMul:in0"], ["MatMul:out0", "Add:in0"], ["Constant_0:out0", "MatMul:in1"],
     ["Constant_1:out0", "Add:in1"]],
"src_in_anchor": [["I:out0", "Reshape:in0"]],
"src_out_tensor": ["Add:out0"],
"acu_lys_alias": ["fullconnect"],
"src_acu_in_tensor_map": [["I:out0", "fullconnect:in0"]],
"src_acu_out_tensor_map": [["Add:out0", "fullconnect:out0"]],
"param_map": {"fullconnect":
                  {"weights": ["INT", "CODE", "self.shape_pick(tensor['Constant_0:out0'])[0]"],
                   "bias": ["BOOL", "VALUE", True]}},
"blob_map": {"fullconnect":
                 {"weight": ["CODE", "self.fc_weight(tensor['Constant_0:out0'], node['MatMul'])"],
                  "bias": ["CODE", "self.tensor_to_numpy(tensor['Constant_1:out0'])"]}},
"acu_inter_flow": [],
"priority_tip": 0,
"pre_condition": None,
"src_ops_main_version": None,
"src_ops_minior_version": [1, 4]}
ruler_list.append(r_rsp_mm_add)

r_rsp_mm_add_v5 = {
"ruler_name": "r_rsp_mm_add_v5",
"src_ops_alias": ["Reshape", "MatMul", "Add", "Constant_0", "Constant_1", "Constant_2"],
"src_inter_flow":
    [["Reshape:out0", "MatMul:in0"], ["MatMul:out0", "Add:in0"], ["Constant_0:out0", "MatMul:in1"],
     ["Constant_1:out0", "Add:in1"], ["Constant_2:out0", "Reshape:in1"]],
"src_in_anchor": [["I:out0", "Reshape:in0"]],
"src_out_tensor": ["Add:out0"],
"acu_lys_alias": ["fullconnect"],
"src_acu_in_tensor_map": [["I:out0", "fullconnect:in0"]],
"src_acu_out_tensor_map": [["Add:out0", "fullconnect:out0"]],
"param_map":
    {"fullconnect":
         {"weights": ["INT", "CODE", "self.shape_pick(tensor['Constant_0:out0'])[0]"],
          "bias": ["BOOL", "VALUE", True]}},
"blob_map":
    {"fullconnect":
         {"weight": ["CODE", "self.fc_weight(tensor['Constant_0:out0'], node['MatMul'])"],
          "bias": ["CODE", "self.tensor_to_numpy(tensor['Constant_1:out0'])"]}},
"acu_inter_flow": [],
"priority_tip": 0,
"pre_condition": None,
"src_ops_main_version": None,
"src_ops_minior_version": [5, -1]}
ruler_list.append(r_rsp_mm_add_v5)

r_mm_add = {
"ruler_name": "r_mm_add",
"src_ops_alias": ["MatMul", "Add", "Constant_0", "Constant_1"],
"src_inter_flow": [["MatMul:out0", "Add:in0"], ["Constant_0:out0", "MatMul:in1"], ["Constant_1:out0", "Add:in1"]],
"src_in_anchor": [["I:out0", "MatMul:in0"]],
"src_out_tensor": ["Add:out0"],
"acu_lys_alias": ["fullconnect"],
"src_acu_in_tensor_map": [["I:out0", "fullconnect:in0"]],
"src_acu_out_tensor_map": [["Add:out0", "fullconnect:out0"]],
"param_map": {"fullconnect":
                  {"weights": ["INT", "CODE", "self.shape_pick(tensor['Constant_0:out0'])[0]"],
                   "bias": ["BOOL", "VALUE", True]}},
"blob_map": {"fullconnect":
                 {"weight": ["CODE", "self.fc_weight(tensor['Constant_0:out0'], node['MatMul'])"],
                  "bias": ["CODE", "self.tensor_to_numpy(tensor['Constant_1:out0'])"]}},
"acu_inter_flow": [],
"priority_tip": 0,
"pre_condition": None,
"src_ops_main_version": None,
"src_ops_minior_version": None}
ruler_list.append(r_mm_add)

r_mm = {
"ruler_name": "r_mm",
"src_ops_alias": ["MatMul", "Constant_0"],
"src_inter_flow": [["Constant_0:out0", "MatMul:in1"]],
"src_in_anchor": [["I:out0", "MatMul:in0"]],
"src_out_tensor": ["MatMul:out0"],
"acu_lys_alias": ["fullconnect"],
"src_acu_in_tensor_map": [["I:out0", "fullconnect:in0"]],
"src_acu_out_tensor_map": [["MatMul:out0", "fullconnect:out0"]],
"param_map": {"fullconnect":
                  {"weights": ["INT", "CODE", "self.shape_pick(tensor['Constant_0:out0'])[0]"],
                   "bias": ["BOOL", "VALUE", "False"]}},
"blob_map": {"fullconnect":
                 {"weight": ["CODE", "self.fc_weight(tensor['Constant_0:out0'], node['MatMul'])"]}},
"acu_inter_flow": [],
"priority_tip": 0,
"pre_condition": None,
"src_ops_main_version": None,
"src_ops_minior_version": None}
ruler_list.append(r_mm)

r_gemm_2_fc = {
"ruler_name": "r_gemm_2_fc",
"src_ops_alias": ["Gemm", "Constant_0"],
"src_inter_flow": [["Constant_0:out0", "Gemm:in1"]],
"src_in_anchor": [["I:out0", "Gemm:in0"]],
"src_out_tensor": ["Gemm:out0"],
"acu_lys_alias": ["fullconnect"],
"src_acu_in_tensor_map": [["I:out0", "fullconnect:in0"]],
"src_acu_out_tensor_map": [["Gemm:out0", "fullconnect:out0"]],
"param_map": {"fullconnect":
                  {"weights": ["INT", "CODE", "self.shape_pick(tensor['Constant_0:out0'])[0]"],
                   "bias": ["BOOL", "VALUE", "False"]}},
"blob_map": {"fullconnect": {"weight": ["CODE", "self.fc_weight(tensor['Constant_0:out0'], node['Gemm'])"]}},
"acu_inter_flow": [],
"priority_tip": 0,
"pre_condition":
    "len(self.query_inputs(node['Gemm'])) == 2 and "\
    "self.attr_pick(node['Gemm'], 'transA', 0) == 0 and "\
    "self.attr_pick(node['Gemm'], 'transB', 0) == 1",
"src_ops_main_version": None,
"src_ops_minior_version": [1, -1]}
ruler_list.append(r_gemm_2_fc)

r_gemm_2_fc_wb = {
"ruler_name": "r_gemm_2_fc_wb",
"src_ops_alias": ["Gemm", "Constant_0", "Constant_1"],
"src_inter_flow": [["Constant_0:out0", "Gemm:in1"], ["Constant_1:out0", "Gemm:in2"]],
"src_in_anchor": [["I:out0", "Gemm:in0"]],
"src_out_tensor": ["Gemm:out0"],
"acu_lys_alias": ["fullconnect"],
"src_acu_in_tensor_map": [["I:out0", "fullconnect:in0"]],
"src_acu_out_tensor_map": [["Gemm:out0", "fullconnect:out0"]],
"param_map": {"fullconnect":
                  {"weights": ["INT", "CODE",
                               "self.gemm_weights_param(tensor['Constant_0:out0'], node['Gemm'], 'transB')"],
                   "bias": ["BOOL", "VALUE", True]}},
"blob_map":
    {"fullconnect":
         {"weight": ["CODE",
                     "self.gemm_weight_blob(tensor['Constant_0:out0'], node['Gemm'], 'transB')"],
          "bias":
              ["CODE",
               "self.tensor_to_numpy(tensor['Constant_1:out0']) * self.attr_pick(node['Gemm'], 'beta', 1.0)"]
          }
     },
"acu_inter_flow": [],
"priority_tip": 0,
"pre_condition":
    "len(self.query_inputs(node['Gemm'])) == 3 and "\
    "self.attr_pick(node['Gemm'], 'transA', 0) == 0 and "\
    "self.attr_pick(node['Gemm'], 'transB', 0) == 1",
"src_ops_main_version": None,
"src_ops_minior_version": [1, -1]}
ruler_list.append(r_gemm_2_fc_wb)

r_gemm_2_fc_wb_notranspose = {
"ruler_name": "gemm_2_fc_notranspose",
"src_ops_alias": ["Gemm", "Constant", "Constant_1"],
"src_inter_flow": [["Constant:out0", "Gemm:in1"], ["Constant_1:out0", "Gemm:in2"]],
"src_in_anchor": [["I_0:out0", "Gemm:in0"]],
"src_out_tensor": ["Gemm:out0"],
"acu_lys_alias": ["fullconnect"],
"src_acu_in_tensor_map": [["I_0:out0", "fullconnect:in0"]],
"src_acu_out_tensor_map": [["Gemm:out0", "fullconnect:out0"]],
"acu_inter_flow": [],
"param_map": {
        "fullconnect":{
            "weights": ["INT", "CODE", "self.gemm_weights_param(tensor['Constant:out0'], node['Gemm'], 'transB')"],
            "bias": ["BOOL", "VALUE", True]
        }
    },
"blob_map":
    {"fullconnect":
         {"weight":
              ["CODE",
               "self.gemm_weight_blob(tensor['Constant:out0'], node['Gemm'], 'transB')"],
          "bias":
              ["CODE",
               "self.tensor_to_numpy(tensor['Constant_1:out0']) * self.attr_pick(node['Gemm'], 'beta', 1.0)"]
          }
     },
"priority_tip": 0,
"pre_condition": "self.attr_pick(node['Gemm'], 'transA', 0) == 0 and"\
    " self.attr_pick(node['Gemm'], 'transB', 0) == 0",
"src_ops_main_version": None,
"src_ops_minior_version": [1, -1]}
ruler_list.append(r_gemm_2_fc_wb_notranspose)

r_gemm_2_fc_4d_wb_notranspose = {
"ruler_name": "r_gemm_2_fc_4d_wb_notranspose",
"src_ops_alias": ["Gemm", "Reshape", "Reshape_1", "Constant", "Constant_1"],
"src_inter_flow": [["Reshape:out0", "Gemm:in0"], ["Reshape_1:out0", "Gemm:in1"], ["Constant:out0", "Gemm:in2"],
                   ["Constant_1:out0", "Reshape_1:in0"]],
"src_in_anchor": [["I_0:out0", "Reshape:in0"]],
"src_out_tensor": ["Gemm:out0"],
"acu_lys_alias": ["fullconnect"],
"src_acu_in_tensor_map": [["I_0:out0", "fullconnect:in0"]],
"src_acu_out_tensor_map": [["Gemm:out0", "fullconnect:out0"]],
"acu_inter_flow": [],
"param_map": {"fullconnect":
                  {"weights": ["INT", "CODE",
                               "self.gemm_weights_param(tensor['Constant:out0'], node['Gemm'], 'transB')"],
                   "bias": ["BOOL", "VALUE", True]}},
"blob_map":
    {"fullconnect":
         {"weight":
              ["CODE",
               "self.gemm_weight_blob(tensor['Reshape_1:out0'], node['Gemm'], 'transB')"],
          "bias":
              ["CODE",
               "self.tensor_to_numpy(tensor['Constant:out0']) * self.attr_pick(node['Gemm'], 'beta', 1.0)"]
          }
     },
"priority_tip": 0,
"pre_condition": "self.attr_pick(node['Gemm'], 'transA', 0) == 0 and"\
    " self.attr_pick(node['Gemm'], 'transB', 0) == 0",
"src_ops_main_version": None,
"src_ops_minior_version": [1, 5]}
ruler_list.append(r_gemm_2_fc_4d_wb_notranspose)

r_gemm_2_fc_4d_wb_notranspose_v5 = {
"ruler_name": "r_gemm_2_fc_4d_wb_notranspose_v5",
"src_ops_alias": ["Gemm", "Reshape", "Reshape_1", "Constant", "Constant_1", "Constant_2", "Constant_3"],
"src_inter_flow": [["Reshape:out0", "Gemm:in0"], ["Reshape_1:out0", "Gemm:in1"], ["Constant:out0", "Gemm:in2"],
                   ["Constant_1:out0", "Reshape:in1"], ["Constant_2:out0", "Reshape_1:in0"],
                   ["Constant_3:out0", "Reshape_1:in1"]],
"src_in_anchor": [["I_0:out0", "Reshape:in0"]],
"src_out_tensor": ["Gemm:out0"],
"acu_lys_alias": ["fullconnect"],
"src_acu_in_tensor_map": [["I_0:out0", "fullconnect:in0"]],
"src_acu_out_tensor_map": [["Gemm:out0", "fullconnect:out0"]],
"acu_inter_flow": [],
"param_map": {"fullconnect":
                  {"weights": ["INT", "CODE",
                               "self.gemm_weights_param(tensor['Reshape_1:out0'], node['Gemm'], 'transB')"],
                   "bias": ["BOOL", "VALUE", True]}},
"blob_map":
    {"fullconnect":
         {"weight":
              ["CODE",
               "self.gemm_weight_blob(tensor['Constant_2:out0'], node['Gemm'], 'transB', \
                    self.shape_pick(tensor['Reshape_1:out0']))"],
          "bias":
              ["CODE",
               "self.tensor_to_numpy(tensor['Constant:out0']) * self.attr_pick(node['Gemm'], 'beta', 1.0)"]
          }
     },
"priority_tip": 0,
"pre_condition": "self.attr_pick(node['Gemm'], 'transA', 0) == 0 and"\
    " self.attr_pick(node['Gemm'], 'transB', 0) == 0",
"src_ops_main_version": None,
"src_ops_minior_version": [5, -1]}
ruler_list.append(r_gemm_2_fc_4d_wb_notranspose_v5)

r_gemm_2_fc_wb_bc = {
"ruler_name": "r_gemm_2_fc_wb_bc",
"src_ops_alias": ["Gemm", "Constant_0", "Constant_1"],
"src_inter_flow": [["Constant_0:out0", "Gemm:in1"], ["Constant_1:out0", "Gemm:in2"]],
"src_in_anchor": [["I:out0", "Gemm:in0"]],
"src_out_tensor": ["Gemm:out0"],
"acu_lys_alias": ["fullconnect"],
"src_acu_in_tensor_map": [["I:out0", "fullconnect:in0"]],
"src_acu_out_tensor_map": [["Gemm:out0", "fullconnect:out0"]],
"param_map": {"fullconnect":
                  {"weights": ["INT", "CODE",
                               "self.gemm_weights_param(tensor['Constant_0:out0'], node['Gemm'], 'transB')"],
                   "bias": ["BOOL", "VALUE", True]}},
"blob_map":
    {"fullconnect":
         {"weight": ["CODE", "self.gemm_weight_blob(tensor['Constant_0:out0'], node['Gemm'], 'transB')"],
          "bias":
              ["CODE",
               "np.ones(self.shape_pick(tensor['Constant_0:out0'])[0], dtype=np.float32)*\
               self.tensor_to_numpy(tensor['Constant_1:out0']) * self.attr_pick(node['Gemm'], 'beta', 1.0)"]
          }
     },
"acu_inter_flow": [],
"priority_tip": 1,
"pre_condition":
    "len(self.query_inputs(node['Gemm'])) == 3 and "\
    "self.attr_pick(node['Gemm'], 'transA', 0) == 0 and "\
    "self.attr_pick(node['Gemm'], 'transB', 0) == 1 and "\
    "self.shape_pick(tensor['Constant_1:out0'])[0] == 1 ",
"src_ops_main_version": None,
"src_ops_minior_version": [1, -1]}
ruler_list.append(r_gemm_2_fc_wb_bc)

r_fullconnect_with_wbrsp = {
"ruler_name": "r_fullconnect_with_wbrsp",
"src_ops_alias": ["Gemm", "Reshape", "Constant", "Constant_1"],
"src_inter_flow": [["Reshape:out0", "Gemm:in1"], ["Constant:out0", "Gemm:in2"], ["Constant_1:out0", "Reshape:in0"]],
"src_in_anchor": [["I_0:out0", "Gemm:in0"]],
"src_out_tensor": ["Gemm:out0"],
"acu_lys_alias": ["fullconnect"],
"src_acu_in_tensor_map": [["I_0:out0", "fullconnect:in0"]],
"src_acu_out_tensor_map": [["Gemm:out0", "fullconnect:out0"]],
"acu_inter_flow": [],
"param_map": {"fullconnect":
                  {"weights": ["INT", "CODE",
                               "self.gemm_weights_param(tensor['Reshape:out0'], node['Gemm'], 'transB')"],
                   "bias": ["BOOL", "VALUE", True]}},
"blob_map":
    {"fullconnect":
         {"weight":
              ["CODE",
               "self.gemm_weight_blob(tensor['Constant_1:out0'], node['Gemm'], 'transB', \
                    self.shape_pick(tensor['Reshape:out0']))"
               ],
          "bias":
              ["CODE",
               "self.tensor_to_numpy(tensor['Constant:out0']) * self.attr_pick(node['Gemm'], 'beta', 1.0)"]
          }
     },
"priority_tip": 0,
"pre_condition":
    "len(self.query_inputs(node['Gemm'])) == 3 and "\
    "self.attr_pick(node['Gemm'], 'transA', 0) == 0 and "\
    "self.attr_pick(node['Gemm'], 'transB', 0) == 1",
"src_ops_main_version": None,
"src_ops_minior_version": [1, 4]}
#Gemm:Gemm_141;Reshape:Reshape_140;Constant:Initializer_115;Constant_1:Initializer_114
ruler_list.append(r_fullconnect_with_wbrsp)

r_fullconnect_with_weight_at_in0_with_reshape = {
"ruler_name": "fc_w@in0_t@in1_with_reshape",
"src_ops_alias": ["Gemm", "Reshape", "Constant", "Constant_1", "Constant_2"],
"src_inter_flow": [["Reshape:out0", "Gemm:in1"], ["Constant:out0", "Gemm:in2"],
                   ["Constant_1:out0", "Reshape:in0"], ["Constant_2:out0", "Reshape:in1"]],
"src_in_anchor": [["I_0:out0", "Gemm:in0"]],
"src_out_tensor": ["Gemm:out0"],
"acu_lys_alias": ["fullconnect"],
"src_acu_in_tensor_map": [["I_0:out0", "fullconnect:in0"]],
"src_acu_out_tensor_map": [["Gemm:out0", "fullconnect:out0"]],
"acu_inter_flow": [],
"param_map": {"fullconnect":
                  {"weights": ["INT", "CODE",
                               "self.gemm_weights_param(tensor['Reshape:out0'], node['Gemm'], 'transB')"],
                   "bias": ["BOOL", "VALUE", True]}},
"blob_map":
    {"fullconnect":
         {"weight":
              ["CODE",
               "self.gemm_weight_blob(tensor['Constant_1:out0'], node['Gemm'], 'transB', \
                    self.shape_pick(tensor['Reshape:out0']))"
               ],
          "bias":
              ["CODE",
               "self.tensor_to_numpy(tensor['Constant:out0']) * self.attr_pick(node['Gemm'], 'beta', 1.0)"]
          }
     },
"priority_tip": 0,
"pre_condition":
    "len(self.query_inputs(node['Gemm'])) == 3 and "\
    "self.attr_pick(node['Gemm'], 'transA', 0) == 0 and "\
    "self.attr_pick(node['Gemm'], 'transB', 0) == 1",
"src_ops_main_version": None,
"src_ops_minior_version": [5, -1]}
#Gemm:Gemm_142;Reshape:Reshape_141;Constant:Initializer_114;Constant_1:Initializer_115;Constant_2:Initializer_117
ruler_list.append(r_fullconnect_with_weight_at_in0_with_reshape)


r_tanh = {
"ruler_name": "r_tanh",
"src_ops_alias": ["Tanh"],
"src_inter_flow": [],
"src_in_anchor": [["I:out0", "Tanh:in0"]],
"src_out_tensor": ["Tanh:out0"],
"acu_lys_alias": ["tanh"],
"src_acu_in_tensor_map": [["I:out0", "tanh:in0"]],
"src_acu_out_tensor_map": [["Tanh:out0", "tanh:out0"]],
"param_map": {},
"blob_map": {},
"acu_inter_flow": [],
"priority_tip": 0,
"pre_condition": None,
"src_ops_main_version": None,
"src_ops_minior_version": None}
ruler_list.append(r_tanh)

r_relu = {
"ruler_name": "r_relu",
"src_ops_alias": ["Relu"],
"src_inter_flow": [],
"src_in_anchor": [["I:out0", "Relu:in0"]],
"src_out_tensor": ["Relu:out0"],
"acu_lys_alias": ["relu"],
"src_acu_in_tensor_map": [["I:out0", "relu:in0"]],
"src_acu_out_tensor_map": [["Relu:out0", "relu:out0"]],
"param_map": {},
"blob_map": {},
"acu_inter_flow": [],
"priority_tip": 0,
"pre_condition": None,
"src_ops_main_version": None,
"src_ops_minior_version": None}
ruler_list.append(r_relu)

r_elu = {
"ruler_name": "r_elu",
"src_ops_alias": ["Elu"],
"src_inter_flow": [],
"src_in_anchor": [["I:out0", "Elu:in0"]],
"src_out_tensor": ["Elu:out0"],
"acu_lys_alias": ["elu"],
"src_acu_in_tensor_map": [["I:out0", "elu:in0"]],
"src_acu_out_tensor_map": [["Elu:out0", "elu:out0"]],
"param_map": {"elu": {"alpha": ["FLOAT", "CODE", "self.attr_pick(node['Elu'], 'alpha', 0.1)"]}},
"blob_map": {},
"acu_inter_flow": [],
"priority_tip": 0,
"pre_condition": None,
"src_ops_main_version": None,
"src_ops_minior_version": [6, -1]}
ruler_list.append(r_elu)

r_sigmoid = {
"ruler_name": "r_sigmoid",
"src_ops_alias": ["Sigmoid"],
"src_inter_flow": [],
"src_in_anchor": [["I:out0", "Sigmoid:in0"]],
"src_out_tensor": ["Sigmoid:out0"],
"acu_lys_alias": ["Sigmoid"],
"src_acu_in_tensor_map": [["I:out0", "Sigmoid:in0"]],
"src_acu_out_tensor_map": [["Sigmoid:out0", "Sigmoid:out0"]],
"param_map": {},
"blob_map": {},
"acu_inter_flow": [],
"priority_tip": 0,
"pre_condition": None,
"src_ops_main_version": None,
"src_ops_minior_version": None}
ruler_list.append(r_sigmoid)

r_leakrelu = {
"ruler_name": "r_leakrelu",
"src_ops_alias": ["LeakyRelu"],
"src_inter_flow": [],
"src_in_anchor": [["I:out0", "LeakyRelu:in0"]],
"src_out_tensor": ["LeakyRelu:out0"],
"acu_lys_alias": ["leakyrelu"],
"src_acu_in_tensor_map": [["I:out0", "leakyrelu:in0"]],
"src_acu_out_tensor_map": [["LeakyRelu:out0", "leakyrelu:out0"]],
"param_map": {"leakyrelu": {"leaky_ratio": ["FLOAT", "CODE", "self.attr_pick(node['LeakyRelu'], 'alpha', 0.01)"]}},
"blob_map": {},
"acu_inter_flow": [],
"priority_tip": 0,
"pre_condition": None,
"src_ops_main_version": None,
"src_ops_minior_version": None}
ruler_list.append(r_leakrelu)

r_prelu_with_reshape = {
"ruler_name": "r_prelu_with_reshape",
"src_ops_alias": ["PRelu", "Reshape", "Constant", "Constant_1"],
"src_inter_flow": [["Reshape:out0", "PRelu:in1"],
                   ["Constant:out0", "Reshape:in0"],
                   ["Constant_1:out0", "Reshape:in1"]],
"src_in_anchor": [["I_0:out0", "PRelu:in0"]],
"src_out_tensor": ["PRelu:out0"],
"acu_lys_alias": ["prelu"],
"src_acu_in_tensor_map": [["I_0:out0", "prelu:in0"]],
"src_acu_out_tensor_map": [["PRelu:out0", "prelu:out0"]],
"acu_inter_flow": [],
"param_map": {"prelu": {}},
"blob_map": {"prelu": {"a":
              ["CODE",
               "np.squeeze(self.tensor_to_numpy(tensor['Reshape:out0']))"]
          }},
"priority_tip": 0,
"pre_condition": None,
"src_ops_main_version": None,
"src_ops_minior_version": [5, -1]}
ruler_list.append(r_prelu_with_reshape)

r_prelu = {
"ruler_name": "r_prelu",
"src_ops_alias": ["PRelu", "Constant_0"],
"src_inter_flow": [ ["Constant_0:out0", "PRelu:in1"]],
"src_in_anchor": [["I:out0", "PRelu:in0"]],
"src_out_tensor": ["PRelu:out0"],
"acu_lys_alias": ["prelu"],
"src_acu_in_tensor_map": [["I:out0", "prelu:in0"]],
"src_acu_out_tensor_map": [["PRelu:out0", "prelu:out0"]],
"param_map": {},
"blob_map": {"prelu": {"a": ["CODE", "self.tensor_to_numpy(tensor['Constant_0:out0'])"]}},
"acu_inter_flow": [],
"priority_tip": 0,
"pre_condition": None,
"src_ops_main_version": None,
"src_ops_minior_version": None}
ruler_list.append(r_prelu)

r_prelu_unsqueeze = {
"ruler_name": "r_prelu_unsqueeze",
"src_ops_alias": ["PRelu", "Constant", "Unsqueeze"],
"src_inter_flow": [["Constant:out0","Unsqueeze:in0"], ["Unsqueeze:out0", "PRelu:in1"]],
"src_in_anchor": [["I:out0", "PRelu:in0"]],
"src_out_tensor": ["PRelu:out0"],
"acu_lys_alias": ["prelu"],
"src_acu_in_tensor_map": [["I:out0", "prelu:in0"]],
"src_acu_out_tensor_map": [["PRelu:out0", "prelu:out0"]],
"param_map":  {},
"blob_map": {"prelu": {"a": ["CODE", "self.tensor_to_numpy(tensor['Unsqueeze:out0'])"]}},
"acu_inter_flow": [],
"priority_tip": 0,
"pre_condition": None,
"src_ops_main_version": None,
"src_ops_minior_version": None}
ruler_list.append(r_prelu_unsqueeze)

r_conv1d = {
"ruler_name": "r_conv1d",
"src_ops_alias": ["Conv", "Constant_0", "Constant_1"],
"src_inter_flow": [["Constant_0:out0", "Conv:in1"], ["Constant_1:out0", "Conv:in2"]],
"src_in_anchor": [["I:out0", "Conv:in0"]],
"src_out_tensor": ["Conv:out0"],
"acu_lys_alias": ["conv1d"],
"src_acu_in_tensor_map": [["I:out0", "conv1d:in0"]],
"src_acu_out_tensor_map": [["Conv:out0", "conv1d:out0"]],
"param_map":
{
"conv1d":
{
"weights": ["INT", "CODE", "self.shape_pick(tensor['Constant_0:out0'])[0]"],
"bias": ["BOOL", "VALUE", True],
"pad_method": ["STRING", "CODE", "'auto' if self.attr_pick(node['Conv'], 'pads', None)"\
               "== None else 'padding_const'"],
"ksize": ["INT", "CODE", "self.attr_pick(node['Conv'], 'kernel_shape')[0]"],
"group_number": ["INT", "CODE", "self.attr_pick(node['Conv'], 'group', 1)"],
"stride": ["INT", "CODE", "self.attr_pick(node['Conv'], 'strides')[0]"],
"dilation":
[
  "INT",
  "CODE",
  "self.attr_pick(node['Conv'], 'dilations')"\
  " if isinstance(self.attr_pick(node['Conv'], 'dilations'), int)"\
  " else self.attr_pick(node['Conv'], 'dilations')[0]"
],
"padding":
[
  "STRING",
  "CODE",
  "'SAME' if self.attr_pick(node['Conv'], 'auto_pad', 'NOTSET') "\
  "in ['SAME_UPPER', 'SAME_LOWER'] else 'VALID' "
],
"pad":
[
  "INTS",
  "CODE",
  "[p for p in self.array_layout(self.attr_pick(node['Conv'], 'pads', [0, 0]), [0, 1])]"
]
}
},
"blob_map":
{
  "conv1d":
  {
    "weight": ["CODE", "self.tensor_to_numpy(tensor['Constant_0:out0'])"],
    "bias": ["CODE", "self.tensor_to_numpy(tensor['Constant_1:out0'])"]
  }
},
"acu_inter_flow": [],
"priority_tip": 0,
"pre_condition": "len(self.shape_pick(tensor['Constant_0:out0'])) == 3",
"src_ops_main_version": None,
"src_ops_minior_version": None
}
ruler_list.append(r_conv1d)

r_group_conv1d = {
"ruler_name": "r_group_conv1d",
"src_ops_alias": ["Conv", "Constant_0", "Constant_1"],
"src_inter_flow": [["Constant_0:out0", "Conv:in1"], ["Constant_1:out0", "Conv:in2"]],
"src_in_anchor": [["I:out0", "Conv:in0"]],
"src_out_tensor": ["Conv:out0"],
"acu_lys_alias": ["group_conv1d"],
"src_acu_in_tensor_map": [["I:out0", "group_conv1d:in0"]],
"src_acu_out_tensor_map": [["Conv:out0", "group_conv1d:out0"]],
"param_map":
{
"group_conv1d":
{
"weights": ["INT", "CODE", "self.shape_pick(tensor['Constant_0:out0'])[0]"],
"bias": ["BOOL", "VALUE", True],
"pad_method": ["STRING", "CODE", "'auto' if self.attr_pick(node['Conv'], 'pads', None)"\
               "== None else 'padding_const'"],
"ksize": ["INT", "CODE", "self.attr_pick(node['Conv'], 'kernel_shape')[0]"],
"group_number": ["INT", "CODE", "self.attr_pick(node['Conv'], 'group', 1)"],
"stride": ["INT", "CODE", "self.attr_pick(node['Conv'], 'strides')[0]"],
"dilation":
[
  "INT",
  "CODE",
  "self.attr_pick(node['Conv'], 'dilations')"\
  " if isinstance(self.attr_pick(node['Conv'], 'dilations'), int)"\
  " else self.attr_pick(node['Conv'], 'dilations')[0]"
],
"padding":
[
  "STRING",
  "CODE",
  "'SAME' if self.attr_pick(node['Conv'], 'auto_pad', 'NOTSET') "\
  "in ['SAME_UPPER', 'SAME_LOWER'] else 'VALID' "
],
"pad":
[
  "INTS",
  "CODE",
  "[p for p in self.array_layout(self.attr_pick(node['Conv'], 'pads', [0, 0]), [0, 1])]"
]
}
},
"blob_map":
{
  "group_conv1d":
  {
    "weight": ["CODE", "self.tensor_to_numpy(tensor['Constant_0:out0'])"],
    "bias": ["CODE", "self.tensor_to_numpy(tensor['Constant_1:out0'])"]
  }
},
"acu_inter_flow": [],
"priority_tip": 1,
"pre_condition": r_group_conv1d_pre_condition(),
"src_ops_main_version": None,
"src_ops_minior_version": None
}
ruler_list.append(r_group_conv1d)

r_depthwise_conv1d = {
"ruler_name": "r_depthwise_conv1d",
"src_ops_alias": ["Conv", "Constant_0", "Constant_1"],
"src_inter_flow": [["Constant_0:out0", "Conv:in1"], ["Constant_1:out0", "Conv:in2"]],
"src_in_anchor": [["I:out0", "Conv:in0"]],
"src_out_tensor": ["Conv:out0"],
"acu_lys_alias": ["depthwise_conv1d"],
"src_acu_in_tensor_map": [["I:out0", "depthwise_conv1d:in0"]],
"src_acu_out_tensor_map": [["Conv:out0", "depthwise_conv1d:out0"]],
"param_map":
{
"depthwise_conv1d":
{
"weights": ["INT", "CODE", "self.shape_pick(tensor['Constant_0:out0'])[0]"],
"bias": ["BOOL", "VALUE", True],
"pad_method": ["STRING", "CODE", "'auto' if self.attr_pick(node['Conv'], 'pads', None)"\
               "== None else 'padding_const'"],
"ksize": ["INT", "CODE", "self.attr_pick(node['Conv'], 'kernel_shape')[0]"],
"group_number": ["INT", "CODE", "self.attr_pick(node['Conv'], 'group', 1)"],
"multiplier": ["INT", "CODE",
               "int(self.shape_pick(tensor['Constant_0:out0'])[0]/self.shape_pick(tensor['I:out0'])[1])"],
"stride": ["INT", "CODE", "self.attr_pick(node['Conv'], 'strides')[0]"],
"dilation":
[
  "INT",
  "CODE",
  "self.attr_pick(node['Conv'], 'dilations')"\
  " if isinstance(self.attr_pick(node['Conv'], 'dilations'), int)"\
  " else self.attr_pick(node['Conv'], 'dilations')[0]"
],
"padding":
[
  "STRING",
  "CODE",
  "'SAME' if self.attr_pick(node['Conv'], 'auto_pad', 'NOTSET') "\
  "in ['SAME_UPPER', 'SAME_LOWER'] else 'VALID' "
],
"pad":
[
  "INTS",
  "CODE",
  "[p for p in self.array_layout(self.attr_pick(node['Conv'], 'pads', [0, 0]), [0, 1])]"
]
}
},
"blob_map":
{
  "depthwise_conv1d":
  {
    "weight": ["CODE", "self.tensor_to_numpy(tensor['Constant_0:out0'])"],
    "bias": ["CODE", "self.tensor_to_numpy(tensor['Constant_1:out0'])"]
  }
},
"acu_inter_flow": [],
"priority_tip": 2,
"pre_condition": r_depthwise_conv1d_pre_condition(),
"src_ops_main_version": None,
"src_ops_minior_version": None
}
ruler_list.append(r_depthwise_conv1d)

r_conv1d_no_bias = {
"ruler_name": "r_conv1d_no_bias",
"src_ops_alias": ["Conv", "Constant_0"],
"src_inter_flow": [["Constant_0:out0", "Conv:in1"]],
"src_in_anchor": [["I:out0", "Conv:in0"]],
"src_out_tensor": ["Conv:out0"],
"acu_lys_alias": ["conv1d"],
"src_acu_in_tensor_map": [["I:out0", "conv1d:in0"]],
"src_acu_out_tensor_map": [["Conv:out0", "conv1d:out0"]],
"param_map":
{
"conv1d":
{
"weights": ["INT", "CODE", "self.shape_pick(tensor['Constant_0:out0'])[0]"],
"bias": ["BOOL", "VALUE", False],
"pad_method": ["STRING", "CODE",
"'auto' if self.attr_pick(node['Conv'], 'pads', None) == None else 'padding_const'"],
"ksize": ["INT", "CODE", "self.attr_pick(node['Conv'], 'kernel_shape')[0]"],
"group_number": ["INT", "CODE", "self.attr_pick(node['Conv'], 'group', 1)"],
"stride": ["INT", "CODE", "self.attr_pick(node['Conv'], 'strides')[0]"],
"dilation":
[
  "INT",
  "CODE",
  "self.attr_pick(node['Conv'], 'dilations')"\
  " if isinstance(self.attr_pick(node['Conv'], 'dilations'), int)"\
  " else self.attr_pick(node['Conv'], 'dilations')[0]"
],
"padding":
[
  "STRING",
  "CODE",
  "'SAME' if self.attr_pick(node['Conv'], 'auto_pad', 'NOTSET') "\
  "in ['SAME_UPPER', 'SAME_LOWER'] else 'VALID' "
],
"pad":
[
  "INTS",
  "CODE",
  "[p for p in self.array_layout(self.attr_pick(node['Conv'], 'pads', [0, 0]), [0, 1])]"
]
}
},
"blob_map":
{
  "conv1d":
  {
    "weight": ["CODE", "self.tensor_to_numpy(tensor['Constant_0:out0'])"]
  }
},
"acu_inter_flow": [],
"priority_tip": 0,
"pre_condition": "len(self.shape_pick(tensor['Constant_0:out0'])) == 3",
"src_ops_main_version": None,
"src_ops_minior_version": None
}
ruler_list.append(r_conv1d_no_bias)

r_group_conv1d_no_bias = {
"ruler_name": "r_group_conv1d_no_bias",
"src_ops_alias": ["Conv", "Constant_0"],
"src_inter_flow": [["Constant_0:out0", "Conv:in1"]],
"src_in_anchor": [["I:out0", "Conv:in0"]],
"src_out_tensor": ["Conv:out0"],
"acu_lys_alias": ["group_conv1d"],
"src_acu_in_tensor_map": [["I:out0", "group_conv1d:in0"]],
"src_acu_out_tensor_map": [["Conv:out0", "group_conv1d:out0"]],
"param_map":
{
"group_conv1d":
{
"weights": ["INT", "CODE", "self.shape_pick(tensor['Constant_0:out0'])[0]"],
"bias": ["BOOL", "VALUE", False],
"pad_method": ["STRING", "CODE",
"'auto' if self.attr_pick(node['Conv'], 'pads', None) == None else 'padding_const'"],
"ksize": ["INT", "CODE", "self.attr_pick(node['Conv'], 'kernel_shape')[0]"],
"group_number": ["INT", "CODE", "self.attr_pick(node['Conv'], 'group', 1)"],
"stride": ["INT", "CODE", "self.attr_pick(node['Conv'], 'strides')[0]"],
"dilation":
[
  "INT",
  "CODE",
  "self.attr_pick(node['Conv'], 'dilations')"\
  " if isinstance(self.attr_pick(node['Conv'], 'dilations'), int)"\
  " else self.attr_pick(node['Conv'], 'dilations')[0]"
],
"padding":
[
  "STRING",
  "CODE",
  "'SAME' if self.attr_pick(node['Conv'], 'auto_pad', 'NOTSET') "\
  "in ['SAME_UPPER', 'SAME_LOWER'] else 'VALID' "
],
"pad":
[
  "INTS",
  "CODE",
  "[p for p in self.array_layout(self.attr_pick(node['Conv'], 'pads', [0, 0]), [0, 1])]"
]
}
},
"blob_map":
{
  "group_conv1d":
  {
    "weight": ["CODE", "self.tensor_to_numpy(tensor['Constant_0:out0'])"]
  }
},
"acu_inter_flow": [],
"priority_tip": 1,
"pre_condition": r_group_conv1d_pre_condition(),
"src_ops_main_version": None,
"src_ops_minior_version": None
}
ruler_list.append(r_group_conv1d_no_bias)

r_depthwise_conv1d_no_bias = {
"ruler_name": "r_depthwise_conv1d_no_bias",
"src_ops_alias": ["Conv", "Constant_0"],
"src_inter_flow": [["Constant_0:out0", "Conv:in1"]],
"src_in_anchor": [["I:out0", "Conv:in0"]],
"src_out_tensor": ["Conv:out0"],
"acu_lys_alias": ["depthwise_conv1d"],
"src_acu_in_tensor_map": [["I:out0", "depthwise_conv1d:in0"]],
"src_acu_out_tensor_map": [["Conv:out0", "depthwise_conv1d:out0"]],
"param_map":
{
"depthwise_conv1d":
{
"weights": ["INT", "CODE", "self.shape_pick(tensor['Constant_0:out0'])[0]"],
"bias": ["BOOL", "VALUE", False],
"pad_method": ["STRING", "CODE",
"'auto' if self.attr_pick(node['Conv'], 'pads', None) == None else 'padding_const'"],
"ksize": ["INT", "CODE", "self.attr_pick(node['Conv'], 'kernel_shape')[0]"],
"group_number": ["INT", "CODE", "self.attr_pick(node['Conv'], 'group', 1)"],
"multiplier": ["INT", "CODE",
               "int(self.shape_pick(tensor['Constant_0:out0'])[0]/self.shape_pick(tensor['I:out0'])[1])"],
"stride": ["INT", "CODE", "self.attr_pick(node['Conv'], 'strides')[0]"],
"dilation":
[
  "INT",
  "CODE",
  "self.attr_pick(node['Conv'], 'dilations')"\
  " if isinstance(self.attr_pick(node['Conv'], 'dilations'), int)"\
  " else self.attr_pick(node['Conv'], 'dilations')[0]"
],
"padding":
[
  "STRING",
  "CODE",
  "'SAME' if self.attr_pick(node['Conv'], 'auto_pad', 'NOTSET') "\
  "in ['SAME_UPPER', 'SAME_LOWER'] else 'VALID' "
],
"pad":
[
  "INTS",
  "CODE",
  "[p for p in self.array_layout(self.attr_pick(node['Conv'], 'pads', [0, 0]), [0, 1])]"
]
}
},
"blob_map":
{
  "depthwise_conv1d":
  {
    "weight": ["CODE", "self.tensor_to_numpy(tensor['Constant_0:out0'])"]
  }
},
"acu_inter_flow": [],
"priority_tip": 2,
"pre_condition": r_depthwise_conv1d_pre_condition(),
"src_ops_main_version": None,
"src_ops_minior_version": None
}
ruler_list.append(r_depthwise_conv1d_no_bias)

r_conv = {
"ruler_name": "r_conv",
"src_ops_alias": ["Conv", "Constant_0", "Constant_1"],
"src_inter_flow": [["Constant_0:out0", "Conv:in1"], ["Constant_1:out0", "Conv:in2"]],
"src_in_anchor": [["I:out0", "Conv:in0"]],
"src_out_tensor": ["Conv:out0"],
"acu_lys_alias": ["convolution"],
"src_acu_in_tensor_map": [["I:out0", "convolution:in0"]],
"src_acu_out_tensor_map": [["Conv:out0", "convolution:out0"]],
"param_map":
{"convolution":
{"weights": ["INT", "CODE", "self.shape_pick(tensor['Constant_0:out0'])[0]"],
"pad_method":
   ["STRING", "CODE", "'auto' if self.attr_pick(node['Conv'], 'pads', None) == None else 'padding_const'"],
"bias": ["BOOL", "VALUE", True],
"ksize_w": ["INT", "CODE", "self.attr_pick(node['Conv'], 'kernel_shape')[1]"],
"group_number": ["INT", "CODE", "self.attr_pick(node['Conv'], 'group', 1)"],
"ksize_h": ["INT", "CODE", "self.attr_pick(node['Conv'], 'kernel_shape')[0]"],
"stride_w": ["INT", "CODE", "self.attr_pick(node['Conv'], 'strides')[1]"],
"stride_h": ["INT", "CODE", "self.attr_pick(node['Conv'], 'strides')[0]"],
"dilation":
     ['INT',
      'CODE',
      "self.attr_pick(node['Conv'], 'dilations')"\
      " if isinstance(self.attr_pick(node['Conv'], 'dilations'), int)"\
      " else self.attr_pick(node['Conv'], 'dilations')[0]"],
"padding":
   ["STRING",
    "CODE",
    "'SAME' if self.attr_pick(node['Conv'], 'auto_pad', 'NOTSET') in ['SAME_UPPER', 'SAME_LOWER'] else 'VALID' "],
"pad":
   ["INTS",
    "CODE",
    "[p for p in self.array_layout(self.attr_pick(node['Conv'], 'pads', [ 0, 0, 0, 0]), [0, 2, 1, 3])]"]
}
},
"blob_map": {"convolution":
                 {"weight": ["CODE", "self.tensor_to_numpy(tensor['Constant_0:out0'])"],
                  "bias": ["CODE", "self.tensor_to_numpy(tensor['Constant_1:out0'])"]}},
"acu_inter_flow": [],
"priority_tip": 0,
"pre_condition": None,
"src_ops_main_version": None,
"src_ops_minior_version": None}
ruler_list.append(r_conv)

r_conv2d_op = {
"ruler_name": "r_conv2d_op",
"src_ops_alias": ["Conv"],
"src_inter_flow": [],
"src_in_anchor": [["I_0:out0", "Conv:in0"], ["I_1:out0", "Conv:in1"]],
"src_out_tensor": ["Conv:out0"],
"acu_lys_alias": ["conv2d_op"],
"src_acu_in_tensor_map": [["I_0:out0", "conv2d_op:in0"], ["I_1:out0", "conv2d_op:in1"]],
"src_acu_out_tensor_map": [["Conv:out0", "conv2d_op:out0"]],
"acu_inter_flow": [],
"param_map": {
    "conv2d_op": {
    "padding":
        ["STRING",
         "CODE",
         "'SAME' if self.attr_pick(node['Conv'], 'auto_pad', 'NOTSET') in ['SAME_UPPER', 'SAME_LOWER'] else 'VALID' "],
    "pad":
        ["INTS",
         "CODE",
         "[p for p in self.array_layout(self.attr_pick(node['Conv'], 'pads', [ 0, 0, 0, 0]), [0, 2, 1, 3])]"],
    "group_number": ["INT", "CODE", "self.attr_pick(node['Conv'], 'group', 1)"],
    "stride_w": ["INT", "CODE", "self.attr_pick(node['Conv'], 'strides')[1]"],
    "stride_h": ["INT", "CODE", "self.attr_pick(node['Conv'], 'strides')[0]"],
    "dilation":
        ['INT',
         'CODE',
         "self.attr_pick(node['Conv'], 'dilations')" \
         " if isinstance(self.attr_pick(node['Conv'], 'dilations'), int)" \
         " else self.attr_pick(node['Conv'], 'dilations')[0]"],
}},
"blob_map": {"conv2d_op": {}},
"priority_tip": 0,
"pre_condition": None,
"src_ops_main_version": None,
"src_ops_minior_version": [1, -1]}
ruler_list.append(r_conv2d_op)

r_depthwise_conv2d_op = {
"ruler_name": "r_depthwise_conv2d_op",
"src_ops_alias": ["Conv"],
"src_inter_flow": [],
"src_in_anchor": [["I_0:out0", "Conv:in0"], ["I_1:out0", "Conv:in1"]],
"src_out_tensor": ["Conv:out0"],
"acu_lys_alias": ["depthwise_conv2d_op"],
"src_acu_in_tensor_map": [["I_0:out0", "depthwise_conv2d_op:in0"], ["I_1:out0", "depthwise_conv2d_op:in1"]],
"src_acu_out_tensor_map": [["Conv:out0", "depthwise_conv2d_op:out0"]],
"acu_inter_flow": [],
"param_map": {
    "depthwise_conv2d_op": {
    "padding":
        ["STRING",
         "CODE",
         "'SAME' if self.attr_pick(node['Conv'], 'auto_pad', 'NOTSET') in ['SAME_UPPER', 'SAME_LOWER'] else 'VALID' "],
    "pad":
        ["INTS",
         "CODE",
         "[p for p in self.array_layout(self.attr_pick(node['Conv'], 'pads', [ 0, 0, 0, 0]), [0, 2, 1, 3])]"],
    "ksize_w": ["INT", "CODE", "self.attr_pick(node['Conv'], 'kernel_shape')[1]"],
    "group_number": ["INT", "CODE", "self.attr_pick(node['Conv'], 'group', 1)"],
    "ksize_h": ["INT", "CODE", "self.attr_pick(node['Conv'], 'kernel_shape')[0]"],
    "stride_w": ["INT", "CODE", "self.attr_pick(node['Conv'], 'strides')[1]"],
    "stride_h": ["INT", "CODE", "self.attr_pick(node['Conv'], 'strides')[0]"],
    "dilation":
        ['INT',
         'CODE',
         "self.attr_pick(node['Conv'], 'dilations')" \
         " if isinstance(self.attr_pick(node['Conv'], 'dilations'), int)" \
         " else self.attr_pick(node['Conv'], 'dilations')[0]"],
}},
"blob_map": {"depthwise_conv2d_op": {}},
"priority_tip": 1,
"pre_condition": "self.attr_pick(node['Conv'], 'group', 1) == self.shape_pick(tensor['I_0:out0'])[1]",
"src_ops_main_version": None,
"src_ops_minior_version": [1, -1]}
ruler_list.append(r_depthwise_conv2d_op)

r_conv_nchw_squeeze = {
"ruler_name": "r_conv_nchw_squeeze",
"src_ops_alias": ["Conv", "Constant", "Squeeze", "Constant_1"],
"src_inter_flow": [["Constant:out0", "Conv:in1"], ["Squeeze:out0", "Conv:in2"], ["Constant_1:out0", "Squeeze:in0"]],
"src_in_anchor": [["I_0:out0", "Conv:in0"]],
"src_out_tensor": ["Conv:out0"],
"acu_lys_alias": ["convolution"],
"src_acu_in_tensor_map": [["I_0:out0", "convolution:in0"]],
"src_acu_out_tensor_map": [["Conv:out0", "convolution:out0"]],
"acu_inter_flow": [],
"param_map":
{"convolution":
{"weights": ["INT", "CODE", "self.shape_pick(tensor['Squeeze:out0'])[0]"],
"pad_method":
   ["STRING", "CODE", "'auto' if self.attr_pick(node['Conv'], 'pads', None) == None else 'padding_const'"],
"bias": ["BOOL", "VALUE", True],
"ksize_w": ["INT", "CODE", "self.attr_pick(node['Conv'], 'kernel_shape')[1]"],
"group_number": ["INT", "CODE", "self.attr_pick(node['Conv'], 'group', 1)"],
"ksize_h": ["INT", "CODE", "self.attr_pick(node['Conv'], 'kernel_shape')[0]"],
"stride_w": ["INT", "CODE", "self.attr_pick(node['Conv'], 'strides')[1]"],
"stride_h": ["INT", "CODE", "self.attr_pick(node['Conv'], 'strides')[0]"],
"dilation":
     ['INT',
      'CODE',
      "self.attr_pick(node['Conv'], 'dilations')"\
      " if isinstance(self.attr_pick(node['Conv'], 'dilations'), int)"\
      " else self.attr_pick(node['Conv'], 'dilations')[0]"],
"padding":
   ["STRING",
    "CODE",
    "'SAME' if self.attr_pick(node['Conv'], 'auto_pad', 'NOTSET') in ['SAME_UPPER', 'SAME_LOWER'] else 'VALID' "],
"pad":
   ["INTS",
    "CODE",
    "[p for p in self.array_layout(self.attr_pick(node['Conv'], 'pads', [ 0, 0, 0, 0]), [0, 2, 1, 3])]"]
}
},
"blob_map": {"convolution":
                 {"weight": ["CODE", "self.tensor_to_numpy(tensor['Constant:out0'])"],
                  "bias": ["CODE", "self.tensor_to_numpy(tensor['Squeeze:out0'])"]}},
"priority_tip": 0,
"pre_condition": None,
"src_ops_main_version": None,
"src_ops_minior_version": [6, -1]}
ruler_list.append(r_conv_nchw_squeeze)

r_conv_no_bias = {
"ruler_name": "r_conv_no_bias",
"src_ops_alias": ["Conv", "Constant_0"],
"src_inter_flow": [["Constant_0:out0", "Conv:in1"]],
"src_in_anchor": [["I:out0", "Conv:in0"]],
"src_out_tensor": ["Conv:out0"],
"acu_lys_alias": ["convolution"],
"src_acu_in_tensor_map": [["I:out0", "convolution:in0"]],
"src_acu_out_tensor_map": [["Conv:out0", "convolution:out0"]],
"param_map":
{"convolution":
{"weights": ["INT", "CODE", "self.shape_pick(tensor['Constant_0:out0'])[0]"],
"pad_method":
   ["STRING", "CODE", "'auto' if self.attr_pick(node['Conv'], 'pads', None) == None else 'padding_const'"],
"bias": ["BOOL", "VALUE", False],
"ksize_w": ["INT", "CODE", "self.attr_pick(node['Conv'], 'kernel_shape')[1]"],
"group_number": ["INT", "CODE", "self.attr_pick(node['Conv'], 'group', 1)"],
"ksize_h": ["INT", "CODE", "self.attr_pick(node['Conv'], 'kernel_shape')[0]"],
"stride_w": ["INT", "CODE", "self.attr_pick(node['Conv'], 'strides')[1]"],
"stride_h": ["INT", "CODE", "self.attr_pick(node['Conv'], 'strides')[0]"],
"dilation":
     ['INT',
      'CODE',
      "self.attr_pick(node['Conv'], 'dilations')"\
      " if isinstance(self.attr_pick(node['Conv'], 'dilations'), int)"\
      " else self.attr_pick(node['Conv'], 'dilations')[0]"],
"padding":
   ["STRING",
    "CODE",
    "'SAME' if self.attr_pick(node['Conv'], 'auto_pad', 'NOTSET') in ['SAME_UPPER', 'SAME_LOWER'] else 'VALID' "],
"pad":
   ["INTS",
    "CODE",
    "[p for p in self.array_layout(self.attr_pick(node['Conv'], 'pads', [ 0, 0, 0, 0]), [0, 2, 1, 3])]"]
}
},
"blob_map": {"convolution":
                 {"weight": ["CODE", "self.tensor_to_numpy(tensor['Constant_0:out0'])"],}},
"acu_inter_flow": [],
"priority_tip": 0,
"pre_condition": None,
"src_ops_main_version": None,
"src_ops_minior_version": None}
ruler_list.append(r_conv_no_bias)

r_conv_add = {
"ruler_name": "r_conv_add",
"src_ops_alias": ["Conv", "Add", "Constant_0", "Constant_1"],
"src_inter_flow": [["Conv:out0", "Add:in0"], ["Constant_0:out0", "Conv:in1"], ["Constant_1:out0", "Add:in1"]],
"src_in_anchor": [["I:out0", "Conv:in0"]],
"src_out_tensor": ["Add:out0"],
"acu_lys_alias": ["convolution"],
"src_acu_in_tensor_map": [["I:out0", "convolution:in0"]],
"src_acu_out_tensor_map": [["Add:out0", "convolution:out0"]],
"param_map":
    {"convolution":
         {"weights": ["INT", "CODE", "self.shape_pick(tensor['Constant_0:out0'])[0]"],
          "pad_method":
              ["STRING", "CODE", "'auto' if self.attr_pick(node['Conv'], 'pads', None) == None else 'padding_const'"],
          "bias": ["BOOL", "VALUE", True],
          "ksize_w": ["INT", "CODE", "self.attr_pick(node['Conv'], 'kernel_shape')[1]"],
          "group_number": ["INT", "CODE", "self.attr_pick(node['Conv'], 'group', 1)"],
          "ksize_h": ["INT", "CODE", "self.attr_pick(node['Conv'], 'kernel_shape')[0]"],
          "stride_w": ["INT", "CODE", "self.attr_pick(node['Conv'], 'strides')[1]"],
          "stride_h": ["INT", "CODE", "self.attr_pick(node['Conv'], 'strides')[0]"],
          "dilation":
             ['INT',
              'CODE',
              "self.attr_pick(node['Conv'], 'dilations')"\
              " if isinstance(self.attr_pick(node['Conv'], 'dilations'), int)"\
              " else self.attr_pick(node['Conv'], 'dilations')[0]"],
          "padding":
              ["STRING",
               "CODE",
               "'SAME'"\
               " if self.attr_pick(node['Conv'], 'auto_pad', 'NOTSET') in ['SAME_UPPER', 'SAME_LOWER'] else "\
               "'VALID' "],
          "pad":
              ["INTS",
               "CODE",
               "[p for p in self.array_layout(self.attr_pick(node['Conv'], 'pads', [ 0, 0, 0, 0]), [0, 2, 1, 3])]"]
          }
     },
"blob_map": {"convolution":
                 {"weight": ["CODE", "self.tensor_to_numpy(tensor['Constant_0:out0'])"],
                  "bias": ["CODE", "self.tensor_to_numpy(tensor['Constant_1:out0'])"]}},
"acu_inter_flow": [],
"priority_tip": 0,
"pre_condition": None,
"src_ops_main_version": None,
"src_ops_minior_version": None}
ruler_list.append(r_conv_add)

r_dconvolution = {
"ruler_name": "r_dconvolution",
"src_ops_alias": ["ConvTranspose", "Constant_0", "Constant_1"],
"src_inter_flow": [ ["Constant_0:out0", "ConvTranspose:in1"], ["Constant_1:out0", "ConvTranspose:in2"]],
"src_in_anchor": [["I:out0", "ConvTranspose:in0"]],
"src_out_tensor": ["ConvTranspose:out0"],
"acu_lys_alias": ["deconvolution"],
"src_acu_in_tensor_map": [["I:out0", "deconvolution:in0"]],
"src_acu_out_tensor_map": [["ConvTranspose:out0", "deconvolution:out0"]],
"param_map":
    {"deconvolution":
         {"weights": ['INT', 'PYFUNC', r_get_deconv_weights()],
          "output_shape": ["INTS", "CODE", "self.attr_pick(node['ConvTranspose'], '_out_shape')[0]"],
          "pad_h": ["INT", "CODE", "self.attr_pick(node['ConvTranspose'], 'pads', [0, 0, 0, 0])[0]"],
          "bias": ["BOOL", "VALUE", True],
          "ksize_w": ["INT", "CODE", "self.attr_pick(node['ConvTranspose'], 'kernel_shape')[1]"],
          "group_number": ["INT", "CODE", "self.attr_pick(node['ConvTranspose'], 'group', 1)"],
          "ksize_h": ["INT", "CODE", "self.attr_pick(node['ConvTranspose'], 'kernel_shape')[0]"],
          "stride_w": ["INT", "CODE", "self.attr_pick(node['ConvTranspose'], 'strides')[1]"],
          "stride_h": ["INT", "CODE", "self.attr_pick(node['ConvTranspose'], 'strides')[0]"],
          "padding":
          ["STRING",
           "CODE",
           "'SAME'"\
           " if self.attr_pick(node['ConvTranspose'], 'auto_pad', 'NOTSET') in ['SAME_UPPER', 'SAME_LOWER'] else "\
           "'VALID' "],
          "pad_w": ["INT", "CODE", "self.attr_pick(node['ConvTranspose'], 'pads', [0, 0, 0, 0])[1]"]}},
"blob_map": {"deconvolution": {"weight": ["CODE", "self.tensor_to_numpy(tensor['Constant_0:out0'])"],
                                 "bias": ["CODE", "self.tensor_to_numpy(tensor['Constant_1:out0'])"]}},
"acu_inter_flow": [],
"priority_tip": 0,
"pre_condition": None,
"src_ops_main_version": None,
"src_ops_minior_version": None}
ruler_list.append(r_dconvolution)

r_dconvolution_no_bias = {
"ruler_name": "r_dconvolution_no_bias",
"src_ops_alias": ["ConvTranspose", "Constant_0"],
"src_inter_flow": [ ["Constant_0:out0", "ConvTranspose:in1"]],
"src_in_anchor": [["I:out0", "ConvTranspose:in0"]],
"src_out_tensor": ["ConvTranspose:out0"],
"acu_lys_alias": ["deconvolution"],
"src_acu_in_tensor_map": [["I:out0", "deconvolution:in0"]],
"src_acu_out_tensor_map": [["ConvTranspose:out0", "deconvolution:out0"]],
"param_map":
    {"deconvolution":
         {"weights": ['INT', 'PYFUNC', r_get_deconv_weights()],
          "output_shape": ["INTS", "CODE", "self.attr_pick(node['ConvTranspose'], '_out_shape')[0]"],
          "pad_h": ["INT", "CODE", "self.attr_pick(node['ConvTranspose'], 'pads', [0, 0, 0, 0])[0]"],
          "bias": ["BOOL", "VALUE", False],
          "ksize_w": ["INT", "CODE", "self.attr_pick(node['ConvTranspose'], 'kernel_shape')[1]"],
          "group_number": ["INT", "CODE", "self.attr_pick(node['ConvTranspose'], 'group', 1)"],
          "ksize_h": ["INT", "CODE", "self.attr_pick(node['ConvTranspose'], 'kernel_shape')[0]"],
          "stride_w": ["INT", "CODE", "self.attr_pick(node['ConvTranspose'], 'strides')[1]"],
          "stride_h": ["INT", "CODE", "self.attr_pick(node['ConvTranspose'], 'strides')[0]"],
          "padding":
          ["STRING",
           "CODE",
           "'SAME'"\
           " if self.attr_pick(node['ConvTranspose'], 'auto_pad', 'NOTSET') in ['SAME_UPPER', 'SAME_LOWER'] else "\
           "'VALID' "],
          "pad_w": ["INT", "CODE", "self.attr_pick(node['ConvTranspose'], 'pads', [0, 0, 0, 0])[1]"]}},
"blob_map": {"deconvolution": {"weight": ["CODE", "self.tensor_to_numpy(tensor['Constant_0:out0'])"]}},
"acu_inter_flow": [],
"priority_tip": 0,
"pre_condition": None,
"src_ops_main_version": None,
"src_ops_minior_version": None}
ruler_list.append(r_dconvolution_no_bias)

r_bn_v6 = {
"ruler_name": "r_bn_v6",
"src_ops_alias": ["BatchNormalization", "Constant_0", "Constant_1", "Constant_2", "Constant_3"],
"src_inter_flow": [["Constant_2:out0", "BatchNormalization:in4"], ["Constant_1:out0", "BatchNormalization:in2"],
                   ["Constant_3:out0", "BatchNormalization:in3"], ["Constant_0:out0", "BatchNormalization:in1"]],
"src_in_anchor": [["I:out0", "BatchNormalization:in0"]],
"src_out_tensor": ["BatchNormalization:out0"],
"acu_lys_alias": ["batchnormalize"],
"src_acu_in_tensor_map": [["I:out0", "batchnormalize:in0"]],
"src_acu_out_tensor_map": [["BatchNormalization:out0", "batchnormalize:out0"]],
"param_map": {"batchnormalize":
                  {"eps": ["FLOAT", "CODE", "self.attr_pick(node['BatchNormalization'], 'epsilon', 1e-5)"]}},
"blob_map":
    {"batchnormalize":
         {"gamma": ["CODE", "self.tensor_to_numpy(tensor['Constant_0:out0'])"],
          "beta": ["CODE", "self.tensor_to_numpy(tensor['Constant_1:out0'])"],
          "variance": ["CODE", "self.tensor_to_numpy(tensor['Constant_2:out0'])"],
          "mean": ["CODE", "self.tensor_to_numpy(tensor['Constant_3:out0'])"]}},
"acu_inter_flow": [],
"priority_tip": 0,
"pre_condition": None,
"src_ops_main_version": None,
"src_ops_minior_version": [6, -1]}
ruler_list.append(r_bn_v6)

r_bn_v5 = {
"ruler_name": "r_bn_v5",
"src_ops_alias": ["BatchNormalization", "Constant_0", "Constant_1", "Constant_2", "Constant_3"],
"src_inter_flow": [["Constant_2:out0", "BatchNormalization:in4"], ["Constant_1:out0", "BatchNormalization:in2"],
                   ["Constant_3:out0", "BatchNormalization:in3"], ["Constant_0:out0", "BatchNormalization:in1"]],
"src_in_anchor": [["I:out0", "BatchNormalization:in0"]],
"src_out_tensor": ["BatchNormalization:out0"],
"acu_lys_alias": ["batchnormalize"],
"src_acu_in_tensor_map": [["I:out0", "batchnormalize:in0"]],
"src_acu_out_tensor_map": [["BatchNormalization:out0", "batchnormalize:out0"]],
"param_map":
    {"batchnormalize": {"eps": ["FLOAT", "CODE", "self.attr_pick(node['BatchNormalization'], 'epsilon', 1e-5)"]}},
"blob_map":
{"batchnormalize":
{"gamma":
  ["CODE",
   "None "\
   "if self.attr_pick(node['BatchNormalization'], 'consumed_inputs')[1] == 0 else"\
   " self.tensor_to_numpy(tensor['Constant_0:out0'])"],
"beta":
  ["CODE",
   "None "\
   "if self.attr_pick(node['BatchNormalization'], 'consumed_inputs')[2] == 0 else"\
   " self.tensor_to_numpy(tensor['Constant_1:out0'])"],
"variance":
  ["CODE",
   "None "\
   "if self.attr_pick(node['BatchNormalization'], 'consumed_inputs')[3] == 0 else"\
   " self.tensor_to_numpy(tensor['Constant_2:out0'])"],
"mean":
  ["CODE",
   "None "\
   "if self.attr_pick(node['BatchNormalization'], 'consumed_inputs')[4] == 0 else"\
   " self.tensor_to_numpy(tensor['Constant_3:out0'])"],
}},
"acu_inter_flow": [],
"priority_tip": 0,
"pre_condition": None,
"src_ops_main_version": None,
"src_ops_minior_version": [1, 5]}
ruler_list.append(r_bn_v5)

r_bn_mul_add_v5 = {
"ruler_name": "r_bn_mul_add_v5",
"src_ops_alias": ["BatchNormalization", "Constant_0", "Constant_1", "Constant_2", "Constant_3",
                  "Mul", "Constant_4", "Add", "Constant_5"],
"src_inter_flow": [["Constant_2:out0", "BatchNormalization:in4"], ["Constant_1:out0", "BatchNormalization:in2"],
                   ["Constant_3:out0", "BatchNormalization:in3"], ["Constant_0:out0", "BatchNormalization:in1"],
                   ["BatchNormalization:out0", "Mul:in0"], ["Constant_4:out0", "Mul:in1"],
                   ["Mul:out0", "Add:in0"], ["Constant_5:out0", "Add:in1"]],
"src_in_anchor": [["I:out0", "BatchNormalization:in0"]],
"src_out_tensor": ["Add:out0"],
"acu_lys_alias": ["batchnormalize"],
"src_acu_in_tensor_map": [["I:out0", "batchnormalize:in0"]],
"src_acu_out_tensor_map": [["Add:out0", "batchnormalize:out0"]],
"param_map":
    {"batchnormalize": {"eps": ["FLOAT", "CODE", "self.attr_pick(node['BatchNormalization'], 'epsilon', 1e-5)"]}},
"blob_map":
{"batchnormalize":
{"gamma":
  ["CODE",
   "self.tensor_to_numpy(tensor['Constant_4:out0']) "\
   "if self.attr_pick(node['BatchNormalization'], 'consumed_inputs')[1] == 0 else"\
   " self.tensor_to_numpy(tensor['Constant_0:out0']) * self.tensor_to_numpy(tensor['Constant_4:out0'])"],
"beta":
  ["CODE",
   "self.tensor_to_numpy(tensor['Constant_5:out0']) "\
   "if self.attr_pick(node['BatchNormalization'], 'consumed_inputs')[1] == 0 else"\
   " self.tensor_to_numpy(tensor['Constant_1:out0']) * self.tensor_to_numpy(tensor['Constant_4:out0']) + "\
   "self.tensor_to_numpy(tensor['Constant_5:out0'])"],
"variance":
  ["CODE",
   "None "\
   "if self.attr_pick(node['BatchNormalization'], 'consumed_inputs')[3] == 0 else"\
   " self.tensor_to_numpy(tensor['Constant_2:out0'])"],
"mean":
  ["CODE",
   "None "\
   "if self.attr_pick(node['BatchNormalization'], 'consumed_inputs')[4] == 0 else"\
   " self.tensor_to_numpy(tensor['Constant_3:out0'])"],
}},
"acu_inter_flow": [],
"priority_tip": 0,
"pre_condition": None,
"src_ops_main_version": None,
"src_ops_minior_version": [1, 5]}
ruler_list.append(r_bn_mul_add_v5)

r_bn_mul_add_v6 = {
"ruler_name": "r_bn_mul_add_v6",
"src_ops_alias": ["BatchNormalization", "Constant_0", "Constant_1", "Constant_2", "Constant_3",
                  "Mul", "Constant_4", "Add", "Constant_5"],
"src_inter_flow": [["Constant_2:out0", "BatchNormalization:in4"], ["Constant_1:out0", "BatchNormalization:in2"],
                   ["Constant_3:out0", "BatchNormalization:in3"], ["Constant_0:out0", "BatchNormalization:in1"],
                   ["BatchNormalization:out0", "Mul:in0"], ["Constant_4:out0", "Mul:in1"],
                   ["Mul:out0", "Add:in0"], ["Constant_5:out0", "Add:in1"]],
"src_in_anchor": [["I:out0", "BatchNormalization:in0"]],
"src_out_tensor": ["Add:out0"],
"acu_lys_alias": ["batchnormalize"],
"src_acu_in_tensor_map": [["I:out0", "batchnormalize:in0"]],
"src_acu_out_tensor_map": [["Add:out0", "batchnormalize:out0"]],
"param_map":
    {"batchnormalize": {"eps": ["FLOAT", "CODE", "self.attr_pick(node['BatchNormalization'], 'epsilon', 1e-5)"]}},
"blob_map":
{"batchnormalize":
{"gamma":
  ["CODE",
   " self.tensor_to_numpy(tensor['Constant_0:out0']) * self.tensor_to_numpy(tensor['Constant_4:out0'])"],
"beta":
  ["CODE",
   "self.tensor_to_numpy(tensor['Constant_1:out0']) * self.tensor_to_numpy(tensor['Constant_4:out0']) + "\
   "self.tensor_to_numpy(tensor['Constant_5:out0'])"],
"variance":
  ["CODE",
   " self.tensor_to_numpy(tensor['Constant_2:out0'])"],
"mean":
  ["CODE",
   " self.tensor_to_numpy(tensor['Constant_3:out0'])"],
}},
"acu_inter_flow": [],
"priority_tip": 0,
"pre_condition": None,
"src_ops_main_version": None,
"src_ops_minior_version": [6, -1]}
ruler_list.append(r_bn_mul_add_v6)

r_mul_add_2_bn = {
"ruler_name": "mul_add_2_bn",
"src_ops_alias": ["Add", "Mul", "Constant", "Constant_1"],
"src_inter_flow": [["Mul:out0", "Add:in0"], ["Constant:out0", "Add:in1"], ["Constant_1:out0", "Mul:in1"]],
"src_in_anchor": [["I_0:out0", "Mul:in0"]],
"src_out_tensor": ["Add:out0"],
"acu_lys_alias": ["batchnormalize"],
"src_acu_in_tensor_map": [["I_0:out0", "batchnormalize:in0"]],
"src_acu_out_tensor_map": [["Add:out0", "batchnormalize:out0"]],
"acu_inter_flow": [],
"param_map": {"batchnormalize": {}},
"blob_map": {"batchnormalize": {"mean": ["VALUE", None],
"variance": ["VALUE", None],
"beta": ["CODE", "self.tensor_to_numpy(tensor['Constant:out0'])"],
"gama": ["CODE", "self.tensor_to_numpy(tensor['Constant_1:out0'])"],
}},
"priority_tip": 0,
"pre_condition": None,
"src_ops_main_version": None,
"src_ops_minior_version": [1, -1]}
ruler_list.append(r_mul_add_2_bn)

r_add_mul_unsqueeze_2_bn = {
"ruler_name": 'add_mul_unsqueeze_2_bn',
"src_ops_alias": ["Add", "Mul", "Unsqueeze", "Unsqueeze_1", "Constant", "Constant_1"],
"src_inter_flow": [["Mul:out0", "Add:in0"], ["Unsqueeze:out0", "Add:in1"], ["Unsqueeze_1:out0", "Mul:in1"],
                   ["Constant:out0", "Unsqueeze:in0"], ["Constant_1:out0", "Unsqueeze_1:in0"]],
"src_in_anchor": [["I_0:out0", "Mul:in0"]],
"src_out_tensor": ["Add:out0"],
"acu_lys_alias": ["batchnormalize"],
"src_acu_in_tensor_map": [["I_0:out0", "batchnormalize:in0"]],
"src_acu_out_tensor_map": [["Add:out0", "batchnormalize:out0"]],
"acu_inter_flow": [],
"param_map": {"batchnormalize": {}},
"blob_map": {"batchnormalize": {"mean": ["VALUE", None],
"variance": ["VALUE", None],
"beta": ["CODE", "self.tensor_to_numpy(tensor['Constant:out0'])"],
"gamma": ["CODE", "self.tensor_to_numpy(tensor['Constant_1:out0'])"],
}},
"priority_tip": 0,
"pre_condition": None,
"src_ops_main_version": None,
"src_ops_minior_version": [1, -1]}
ruler_list.append(r_add_mul_unsqueeze_2_bn)

r_maxpool = {
"ruler_name": "r_maxpool",
"src_ops_alias": ["MaxPool"],
"src_inter_flow": [],
"src_in_anchor": [["I:out0", "MaxPool:in0"]],
"src_out_tensor": ["MaxPool:out0"],
"acu_lys_alias": ["pooling"],
"src_acu_in_tensor_map": [["I:out0", "pooling:in0"]],
"src_acu_out_tensor_map": [["MaxPool:out0", "pooling:out0"]],
"param_map": {"pooling":
  {"type": ["STRING", "VALUE", "MAX"],
   "pad_method": ["STRING", "VALUE", "padding_const"],
   "pad_h": ["INT", "CODE", "self.attr_pick(node['MaxPool'], 'pads', [0, 0, 0, 0])[0]"],
   "ksize_w": ["INT", "CODE", "self.attr_pick(node['MaxPool'], 'kernel_shape')[1]"],
   "stride_w": ["INT", "CODE", "self.attr_pick(node['MaxPool'], 'strides')[1]"],
   "ksize_h": ["INT", "CODE", "self.attr_pick(node['MaxPool'], 'kernel_shape')[0]"],
   "round_type": ["STRING", "VALUE", "floor"],
   "stride_h": ["INT", "CODE", "self.attr_pick(node['MaxPool'], 'strides')[0]"],
   "padding":
   ["STRING",
    "CODE",
    "'SAME' if self.attr_pick(node['MaxPool'], 'auto_pad', 'NOTSET') in ['SAME_UPPER', 'SAME_LOWER'] else 'VALID'"
    ],
   "pad":
       ["INTS",
        "CODE",
        "[p for p in self.array_layout(self.attr_pick(node['MaxPool'], 'pads', [ 0, 0, 0, 0]), [0, 2, 1, 3])]"],
   "pad_w": ["INT", "CODE", "self.attr_pick(node['MaxPool'], 'pads', [0, 0, 0, 0])[1]"]}},
"blob_map": {},
"acu_inter_flow": [],
"priority_tip": 0,
"pre_condition": None,
"src_ops_main_version": None,
"src_ops_minior_version": [1, -1]}
ruler_list.append(r_maxpool)

r_avgpool = {
"ruler_name": "r_avgpool",
"src_ops_alias": ["AveragePool"],
"src_inter_flow": [],
"src_in_anchor": [["I:out0", "AveragePool:in0"]],
"src_out_tensor": ["AveragePool:out0"],
"acu_lys_alias": ["pooling"],
"src_acu_in_tensor_map": [["I:out0", "pooling:in0"]],
"src_acu_out_tensor_map": [["AveragePool:out0", "pooling:out0"]],
"param_map": {"pooling":
{"type": ["STRING", "VALUE", "AVG"],
"pad_method": ["STRING", "VALUE", "padding_const"],
"pad_h": ["INT", "CODE", "self.attr_pick(node['AveragePool'], 'pads', [0, 0, 0, 0])[0]"],
"ksize_w": ["INT", "CODE", "self.attr_pick(node['AveragePool'], 'kernel_shape')[1]"],
"stride_w": ["INT", "CODE", "self.attr_pick(node['AveragePool'], 'strides')[1]"],
"ksize_h": ["INT", "CODE", "self.attr_pick(node['AveragePool'], 'kernel_shape')[0]"],
"round_type": ["STRING", "VALUE", "floor"],
"stride_h": ["INT", "CODE", "self.attr_pick(node['AveragePool'], 'strides')[0]"],
"padding":
["STRING",
"CODE",
"'SAME' if self.attr_pick(node['AveragePool'], 'auto_pad', 'NOTSET') in ['SAME_UPPER', 'SAME_LOWER'] else 'VALID'"
],
"pad":
   ["INTS",
    "CODE",
    "[str(p) for p in self.array_layout(self.attr_pick(node['AveragePool'], 'pads', [ 0, 0, 0, 0]), [0, 2, 1, 3])]"
    ],
"pad_w": ["INT", "CODE", "self.attr_pick(node['AveragePool'], 'pads', [0, 0, 0, 0])[1]"]}},
"blob_map": {},
"acu_inter_flow": [],
"priority_tip": 0,
"pre_condition": None,
"src_ops_main_version": None,
"src_ops_minior_version": [1, -1]}
ruler_list.append(r_avgpool)

r_global_avgpool = {
"ruler_name": "r_global_avgpool",
"src_ops_alias": ["GlobalAveragePool"],
"src_inter_flow": [],
"src_in_anchor": [["I:out0", "GlobalAveragePool:in0"]],
"src_out_tensor": ["GlobalAveragePool:out0"],
"acu_lys_alias": ["pooling"],
"src_acu_in_tensor_map": [["I:out0", "pooling:in0"]],
"src_acu_out_tensor_map": [["GlobalAveragePool:out0", "pooling:out0"]],
"param_map": {"pooling":
{"type": ["STRING", "VALUE", "AVG"],
"pad_method": ["STRING", "VALUE", "padding_const"],
"pad_h": ["INT", "CODE", "self.attr_pick(node['GlobalAveragePool'], 'pads', [0, 0, 0, 0])[0]"],
"round_type": ["STRING", "VALUE", "floor"],
"pad":
   ["INTS",
    "CODE",
"[str(p) for p in self.array_layout(self.attr_pick(node['GlobalAveragePool'], 'pads', [ 0, 0, 0, 0]), [0, 2, 1, 3])]"
    ],
"padding": ["STRING", "VALUE", "VALID"],
"global_pooling": ["BOOL", "VALUE", True],
"pad_w": ["INT", "CODE", "self.attr_pick(node['GlobalAveragePool'], 'pads', [0, 0, 0, 0])[1]"]}},
"blob_map": {},
"acu_inter_flow": [],
"priority_tip": 0,
"pre_condition": None,
"src_ops_main_version": None,
"src_ops_minior_version": [1, -1]}
ruler_list.append(r_global_avgpool)

r_rsp_v1 = {
"ruler_name": "r_rsp_v1",
"src_ops_alias": ["Reshape"],
"src_inter_flow": [],
"src_in_anchor": [["I:out0", "Reshape:in0"]],
"src_out_tensor": ["Reshape:out0"],
"acu_lys_alias": ["reshape"],
"src_acu_in_tensor_map": [["I:out0", "reshape:in0"]],
"src_acu_out_tensor_map": [["Reshape:out0", "reshape:out0"]],
"param_map": {"reshape":
                  {"shape":
                       ["STRING", "CODE", "self.shape_pick(tensor['Reshape:out0'])"]
                   }
              },
"blob_map": {},
"acu_inter_flow": [],
"priority_tip": 0,
"pre_condition": None,
"src_ops_main_version": None,
"src_ops_minior_version": [1, 4]}
ruler_list.append(r_rsp_v1)

r_rsp_v5 = {
"ruler_name": "r_rsp_v5",
"src_ops_alias": ["Reshape", "Constant_0"],
"src_inter_flow": [["Constant_0:out0", "Reshape:in1"]],
"src_in_anchor": [["I:out0", "Reshape:in0"]],
"src_out_tensor": ["Reshape:out0"],
"acu_lys_alias": ["reshape"],
"src_acu_in_tensor_map": [["I:out0", "reshape:in0"]],
"src_acu_out_tensor_map": [["Reshape:out0", "reshape:out0"]],
"param_map": {"reshape": {"shape": ["INTS", "CODE", "self.shape_pick(tensor['Reshape:out0'])"]}},
"blob_map": {},
"acu_inter_flow": [],
"priority_tip": 0,
"pre_condition": None,
"src_ops_main_version": None,
"src_ops_minior_version": [5, -1]}
ruler_list.append(r_rsp_v5)

r_rsp_v5x = {
"ruler_name": "r_rsp_v5x",
"src_ops_alias": ["Reshape", "Constant_0", "Constant_1"],
"src_inter_flow": [["Constant_0:out0", "Reshape:in0"], ["Constant_1:out0", "Reshape:in1"]],
"src_in_anchor": [],
"src_out_tensor": ["Reshape:out0"],
"acu_lys_alias": ["variable"],
"src_acu_in_tensor_map": [],
"src_acu_out_tensor_map": [["Reshape:out0", "variable:out0"]],
"param_map": {"variable": {"shape": ['ORIGIN', 'CODE', "self.shape_pick(tensor['Reshape:out0'])"]}},
"blob_map": {"variable": {'data':
                              ['CODE',
                               "np.reshape("\
                               "self.tensor_to_numpy(tensor['Constant_0:out0']), "\
                               "self.tensor_to_numpy(tensor['Constant_1:out0']).astype(np.int32).tolist())"],}},
"acu_inter_flow": [],
"priority_tip": 0,
"pre_condition": None,
"src_ops_main_version": None,
"src_ops_minior_version": [5, -1]}
ruler_list.append(r_rsp_v5x)

r_dynamic_rsp_5x = {
"ruler_name": "r_dynamic_rsp_5x",
"src_ops_alias": ["Reshape"],
"src_inter_flow": [],
"src_in_anchor": [["I:out0", "Reshape:in0"], ["I_1:out0", "Reshape:in1"]],
"src_out_tensor": ["Reshape:out0"],
"acu_lys_alias": ["reshape"],
"src_acu_in_tensor_map": [["I:out0", "reshape:in0"]],
"src_acu_out_tensor_map": [["Reshape:out0", "reshape:out0"]],
"param_map": {"reshape": {"shape": ["INTS", "CODE", "self.attr_pick(node['Reshape'], '_out_shape')[0]"]}},
"blob_map": {},
"acu_inter_flow": [],
"priority_tip": 0,
"pre_condition": None,
"src_ops_main_version": None,
"src_ops_minior_version": [5, -1]}
ruler_list.append(r_dynamic_rsp_5x)

r_squeeze_with_constant = {
"ruler_name": "r_squeeze_with_constant",
"src_ops_alias": ["Squeeze", "Constant_0"],
"src_inter_flow": [],
"src_in_anchor": [["I:out0", "Squeeze:in0"]],
"src_out_tensor": ["Squeeze:out0"],
"acu_lys_alias": ["reshape"],
"src_acu_in_tensor_map": [["I:out0", "reshape:in0"]],
"src_acu_out_tensor_map": [["Squeeze:out0", "reshape:out0"]],
"param_map":
{"reshape":
 {"shape":
  ["INTS",
   "CODE",
   "self.squeeze_shapes(self.attr_pick(node['Squeeze'], 'axes', None), self.shape_pick(tensor['Constant_0:out0']))"
   ]
  }
 },
"blob_map": {},
"acu_inter_flow": [],
"priority_tip": 0,
"pre_condition": None,
"src_ops_main_version": None,
"src_ops_minior_version": [1, -1]}
ruler_list.append(r_squeeze_with_constant)

r_squeeze = {
"ruler_name": "r_squeeze",
"src_ops_alias": ["Squeeze"],
"src_inter_flow": [],
"src_in_anchor": [["I_0:out0", "Squeeze:in0"]],
"src_out_tensor": ["Squeeze:out0"],
"acu_lys_alias": ["squeeze"],
"src_acu_in_tensor_map": [["I_0:out0", "squeeze:in0"]],
"src_acu_out_tensor_map": [["Squeeze:out0", "squeeze:out0"]],
"acu_inter_flow": [],
"param_map":
{"squeeze":
 {"axis_list": ["INTS", "CODE", "self.attr_pick(node['Squeeze'], 'axes')"],
  }
 },
"blob_map": {},
"priority_tip": 0,
"pre_condition": None,
"src_ops_main_version": None,
"src_ops_minior_version": [1, -1]}
ruler_list.append(r_squeeze)

r_unsqueeze = {
"ruler_name": "r_unsqueeze",
"src_ops_alias": ["Unsqueeze"],
"src_inter_flow": [],
"src_in_anchor": [["I_0:out0", "Unsqueeze:in0"]],
"src_out_tensor": ["Unsqueeze:out0"],
"acu_lys_alias": ["reshape"],
"src_acu_in_tensor_map": [["I_0:out0", "reshape:in0"]],
"src_acu_out_tensor_map": [["Unsqueeze:out0", "reshape:out0"]],
"acu_inter_flow": [],
"param_map":
{"reshape":
 {"shape":
  ["INTS",
   "CODE",
   "self.unsqueeze_shape(self.attr_pick(node['Unsqueeze'], 'axes'), self.shape_pick(tensor['I_0:out0']))"
   ]
  }
 },
"blob_map": {},
"priority_tip": 0,
"pre_condition": None,
"src_ops_main_version": None,
"src_ops_minior_version": [1, -1]}
ruler_list.append(r_unsqueeze)

{
"ruler_name": "unsqueezex",
"src_ops_alias": ["Unsqueeze"],
"src_inter_flow": [],
"src_in_anchor": [["I_0:out0", "Unsqueeze:in0"]],
"src_out_tensor": ["Unsqueeze:out0"],
"acu_lys_alias": ["unsqueeze"],
"src_acu_in_tensor_map": [["I_0:out0", "unsqueeze:in0"]],
"src_acu_out_tensor_map": [["Unsqueeze:out0", "unsqueeze:out0"]],
"acu_inter_flow": [],
"param_map": {"unsqueeze": {}},
"blob_map": {"unsqueeze": {}},
"priority_tip": 0,
"pre_condition": None,
"src_ops_main_version": None,
"src_ops_minior_version": [1, -1]}

r_flatten = {
"ruler_name": "r_flatten",
"src_ops_alias": ["Flatten"],
"src_inter_flow": [],
"src_in_anchor": [["I:out0", "Flatten:in0"]],
"src_out_tensor": ["Flatten:out0"],
"acu_lys_alias": ["reshape"],
"src_acu_in_tensor_map": [["I:out0", "reshape:in0"]],
"src_acu_out_tensor_map": [["Flatten:out0", "reshape:out0"]],
"param_map": {"reshape": {"shape": ["INTS", "VALUE", [0, -1]]}},
"blob_map": {},
"acu_inter_flow": [],
"priority_tip": 0,
"pre_condition": None,
"src_ops_main_version": None,
"src_ops_minior_version": [1, -1]}
ruler_list.append(r_flatten)

r_transpose = {
"ruler_name": "r_transpose",
"src_ops_alias": ["Transpose"],
"src_inter_flow": [],
"src_in_anchor": [["I:out0", "Transpose:in0"]],
"src_out_tensor": ["Transpose:out0"],
"acu_lys_alias": ["permute"],
"src_acu_in_tensor_map": [["I:out0", "permute:in0"]],
"src_acu_out_tensor_map": [["Transpose:out0", "permute:out0"]],
"param_map":
    {"permute":
         {"perm": ["STRING", "CODE", "' '.join([str(p) for p in self.attr_pick(node['Transpose'], 'perm')])"]}
     },
"blob_map": {},
"acu_inter_flow": [],
"priority_tip": 0,
"pre_condition": None,
"src_ops_main_version": None,
"src_ops_minior_version": [1, -1]}
ruler_list.append(r_transpose)

def r_concat_templete(concat_count):
    r_concat_dict = {
"ruler_name": "concat_{}".format(concat_count),
"src_ops_alias": ["Concat"],
"src_inter_flow": [],
"src_in_anchor": [["I_{}:out0".format(order), "Concat:in{}".format(order)] for order in range(concat_count)],
"src_out_tensor": ["Concat:out0"],
"acu_lys_alias": ["concat"],
"src_acu_in_tensor_map": [["I_{}:out0".format(order), "concat:in{}".format(order)] for order in range(concat_count)],
"src_acu_out_tensor_map": [["Concat:out0", "concat:out0"]],
"param_map": {"concat": {"dim": ["INT", "CODE", "self.attr_pick(node['Concat'], 'axis')"]}},
"blob_map": {},
"acu_inter_flow": [],
"priority_tip": 0,
"pre_condition": None,
"src_ops_main_version": None,
"src_ops_minior_version": [1, -1]}
    return r_concat_dict

# Add you miss concat size in the list
#for i in [2, 3, 4, 5, 6]:
#    ruler_list.append(r_concat_templete(i))

def r_concat_specail_templete(concat_count, concat_input_list):
    r_concat_specail_dict = {
"ruler_name": "concat_specail_{}".format(concat_count),
"src_ops_alias": ["Concat"],
"src_inter_flow": [],
"src_in_anchor": [["I_{}:out0".format(concat_input_list[order]), "Concat:in{}".format(order)]
                  for order in range(concat_count)],
"src_out_tensor": ["Concat:out0"],
"acu_lys_alias": ["concat"],
"src_acu_in_tensor_map": [["I_{}:out0".format(concat_input_list[order]), "concat:in{}".format(order)]
                          for order in range(concat_count)],
"src_acu_out_tensor_map": [["Concat:out0", "concat:out0"]],
"param_map": {"concat": {"dim": ["INT", "CODE", "self.attr_pick(node['Concat'], 'axis')"]}},
"blob_map": {},
"acu_inter_flow": [],
"priority_tip": 0,
"pre_condition": None,
"src_ops_main_version": None,
"src_ops_minior_version": [1, -1]}
    return r_concat_specail_dict

def r_split_templete(split_count):
    r_split_dict = {
"ruler_name": "r_split_{}".format(split_count),
"src_ops_alias": ["Split"],
"src_inter_flow": [],
"src_in_anchor": [["I:out0", "Split:in0"]],
"src_out_tensor": ["Split:out{}".format(pid) for pid in range(split_count)],
"acu_lys_alias": ["split"],
"src_acu_in_tensor_map": [["I:out0", "split:in0"]],
"src_acu_out_tensor_map": [["Split:out{}".format(pid), "split:out{}".format(pid)] for pid in range(split_count)],
"param_map":
{"split":
     {"slices":
          ["STRING",
           "CODE",
           "self.split_slice_cale(self.attr_pick(node['Split'], 'split'))"],
      "dim": ["INT", "CODE", "self.attr_pick(node['Split'], 'axis', 0)"]}},
"blob_map": {},
"acu_inter_flow": [],
"priority_tip": 0,
"pre_condition": None,
"src_ops_main_version": None,
"src_ops_minior_version": [2, -1]}
    return r_split_dict

# Add you miss split size in the list
#for i in [2, 3, 4, 5, 6]:
#    ruler_list.append(r_split_templete(i))

def r_elemwise_templete(input_count, op):
    operator_map={'Sum':'SUM', 'Max': 'MAX'}
    r_elemwise_x_dict = {
"ruler_name": "r_elemwise_{}_{}".format(op, input_count),
"src_ops_alias": [op],
"src_inter_flow": [],
"src_in_anchor": [["I_{}:out0".format(order), "{}:in{}".format(op, order)] for order in range(input_count)],
"src_out_tensor": ["{}:out0".format(op)],
"acu_lys_alias": ['eltwise'],
"src_acu_in_tensor_map": [["I_{}:out0".format(order), "eltwise:in{}".format(order)] for order in range(input_count)],
"src_acu_out_tensor_map": [["{}:out0".format(op), "eltwise:out0"]],
"param_map": {"eltwise": {"operation": ["STRING", "VALUE", operator_map[op]]}},
"blob_map": {},
"acu_inter_flow": [],
"priority_tip": 0,
"pre_condition": None,
"src_ops_main_version": None,
"src_ops_minior_version": [1, -1]}
    return r_elemwise_x_dict

# Add you miss sum size in the list
#for i in [2, 3, 4, 5, 6]:
#    ruler_list.append(r_elemwise_templete(i, 'Sum'))

#for i in [2, 3, 4, 5, 6]:
#    ruler_list.append(r_elemwise_templete(i, 'Max'))


def r_elemwise_specail_templete(input_count,input_list, op):
    operator_map={'Sum':'SUM', 'Max': 'MAX'}
    r_elemwise_x_specail_dict = {
"ruler_name": "r_elemwise_{}_specail_{}".format(op, input_count),
"src_ops_alias": [op],
"src_inter_flow": [],
"src_in_anchor": [["I_{}:out0".format(input_list[order]), "{}:in{}".format(op, order)]
                  for order in range(input_count)],
"src_out_tensor": ["{}:out0".format(op)],
"acu_lys_alias": ['eltwise'],
"src_acu_in_tensor_map": [["I_{}:out0".format(input_list[order]), "eltwise:in{}".format(order)]
                          for order in range(input_count)],
"src_acu_out_tensor_map": [["{}:out0".format(op), "eltwise:out0"]],
"param_map": {"eltwise": {"operation": ["STRING", "VALUE", operator_map[op]]}},
"blob_map": {},
"acu_inter_flow": [],
"priority_tip": 0,
"pre_condition": None,
"src_ops_main_version": None,
"src_ops_minior_version": [1, -1]}
    return r_elemwise_x_specail_dict

r_softmax = {
"ruler_name": "r_softmax",
"src_ops_alias": ["Softmax"],
"src_inter_flow": [],
"src_in_anchor": [["I:out0", "Softmax:in0"]],
"src_out_tensor": ["Softmax:out0"],
"acu_lys_alias": ["softmax"],
"src_acu_in_tensor_map": [["I:out0", "softmax:in0"]],
"src_acu_out_tensor_map": [["Softmax:out0", "softmax:out0"]],
"param_map": {
"softmax": {"sf_axis": ['INT', 'PYFUNC', r_softmax_get_sf_axis()]}
},
"blob_map": {},
"acu_inter_flow": [],
"priority_tip": 0,
"pre_condition": None,
"src_ops_main_version": None,
"src_ops_minior_version": [1, -1]}
ruler_list.append(r_softmax)


r_log_softmax = {
"ruler_name": "logsoftmax",
"src_ops_alias": ["LogSoftmax"],
"src_inter_flow": [],
"src_in_anchor": [["I_0:out0", "LogSoftmax:in0"]],
"src_out_tensor": ["LogSoftmax:out0"],
"acu_lys_alias": ["log_softmax"],
"src_acu_in_tensor_map": [["I_0:out0", "log_softmax:in0"]],
"src_acu_out_tensor_map": [["LogSoftmax:out0", "log_softmax:out0"]],
"acu_inter_flow": [],
"param_map": {
"log_softmax": {"sf_axis": ['INT', 'PYFUNC', r_softmax_get_log_sf_axis()]}
},
"blob_map": {"log_softmax": {}},
"priority_tip": 0,
"pre_condition": None,
"src_ops_main_version": None,
"src_ops_minior_version": [1, -1]}
ruler_list.append(r_log_softmax)

r_dropout_out2_as_dropout_1_6 = {
"ruler_name": 'dropout_out2_as_dropout_1_6',
"src_ops_alias": ["Dropout"],
"src_inter_flow": [],
"src_in_anchor": [["I_0:out0", "Dropout:in0"]],
"src_out_tensor": ["Dropout:out0", "Dropout:out1"],
"acu_lys_alias": ["dropout"],
"src_acu_in_tensor_map": [["I_0:out0", "dropout:in0"]],
"src_acu_out_tensor_map": [["Dropout:out0", "dropout:out0"]],
"acu_inter_flow": [],
"param_map": {'dropout':{'ratio':['FLOAT', 'CODE', "self.attr_pick(node['Dropout'], 'ratio', 0.5)"]}},
"blob_map": {"dropout": {}},
"priority_tip": 0,
"pre_condition": "self.attr_pick(node['Dropout'], 'is_test', 0) == 0 and "\
                 "math.isclose(0.0, self.attr_pick(node['Dropout'], 'ratio', 0.5)) == False",
"src_ops_main_version": None,
"src_ops_minior_version": [1, 6]}
ruler_list.append(r_dropout_out2_as_dropout_1_6)

r_dropout_out2_as_noop_1_6 = {
"ruler_name": 'dropout_out2_as_noop_1_6',
"src_ops_alias": ["Dropout"],
"src_inter_flow": [],
"src_in_anchor": [["I_0:out0", "Dropout:in0"]],
"src_out_tensor": ["Dropout:out0", "Dropout:out1"],
"acu_lys_alias": ["noop"],
"src_acu_in_tensor_map": [["I_0:out0", "noop:in0"]],
"src_acu_out_tensor_map": [["Dropout:out0", "noop:out0"]],
"acu_inter_flow": [],
"param_map": {"noop": {}},
"blob_map": {"noop": {}},
"priority_tip": 0,
"pre_condition": "self.attr_pick(node['Dropout'], 'is_test', 0) != 0 or "\
                 "math.isclose(0.0, self.attr_pick(node['Dropout'], 'ratio', 0.5)) == True",
"src_ops_main_version": None,
"src_ops_minior_version": [1, 6]}
ruler_list.append(r_dropout_out2_as_noop_1_6)

r_dropout_out1_as_dropout_1_6 = {
"ruler_name": "r_dropout_out1_as_dropout_1_6",
"src_ops_alias": ["Dropout"],
"src_inter_flow": [],
"src_in_anchor": [["I:out0", "Dropout:in0"]],
"src_out_tensor": ["Dropout:out0"],
"acu_lys_alias": ["dropout"],
"src_acu_in_tensor_map": [["I:out0", "dropout:in0"]],
"src_acu_out_tensor_map": [["Dropout:out0", "dropout:out0"]],
"param_map": {'dropout':{'ratio':['FLOAT', 'CODE', "self.attr_pick(node['Dropout'], 'ratio', 0.5)"]}},
"blob_map": {},
"acu_inter_flow": [],
"priority_tip": 0,
"pre_condition": "self.attr_pick(node['Dropout'], 'is_test', 0) == 0 and "\
                 "math.isclose(0.0, self.attr_pick(node['Dropout'], 'ratio', 0.5)) == False",
"src_ops_main_version": None,
"src_ops_minior_version": [1, 6]}
ruler_list.append(r_dropout_out1_as_dropout_1_6)

r_dropout_out1_as_noop_1_6 = {
"ruler_name": "r_dropout_out1_as_noop_1_6",
"src_ops_alias": ["Dropout"],
"src_inter_flow": [],
"src_in_anchor": [["I:out0", "Dropout:in0"]],
"src_out_tensor": ["Dropout:out0"],
"acu_lys_alias": ["noop"],
"src_acu_in_tensor_map": [["I:out0", "noop:in0"]],
"src_acu_out_tensor_map": [["Dropout:out0", "noop:out0"]],
"param_map": {},
"blob_map": {},
"acu_inter_flow": [],
"priority_tip": 0,
"pre_condition": "self.attr_pick(node['Dropout'], 'is_test', 0) != 0 or "\
                 "math.isclose(0.0, self.attr_pick(node['Dropout'], 'ratio', 0.5)) == True",
"src_ops_main_version": None,
"src_ops_minior_version": [1, 6]}
ruler_list.append(r_dropout_out1_as_noop_1_6)

r_dropout_out2_as_dropout_7 = {
"ruler_name": 'dropout_out2_as_dropout_7',
"src_ops_alias": ["Dropout"],
"src_inter_flow": [],
"src_in_anchor": [["I_0:out0", "Dropout:in0"]],
"src_out_tensor": ["Dropout:out0", "Dropout:out1"],
"acu_lys_alias": ["dropout"],
"src_acu_in_tensor_map": [["I_0:out0", "dropout:in0"]],
"src_acu_out_tensor_map": [["Dropout:out0", "dropout:out0"]],
"acu_inter_flow": [],
"param_map": {'dropout':{'ratio':['FLOAT', 'CODE', "self.attr_pick(node['Dropout'], 'ratio', 0.5)"]}},
"blob_map": {"dropout": {}},
"priority_tip": 0,
"pre_condition": "math.isclose(0.0, self.attr_pick(node['Dropout'], 'ratio', 0.5)) == False",
"src_ops_main_version": None,
"src_ops_minior_version": [7, -1]}
ruler_list.append(r_dropout_out2_as_dropout_7)

r_dropout_out2_as_noop_7 = {
"ruler_name": 'dropout_out2_as_noop_7',
"src_ops_alias": ["Dropout"],
"src_inter_flow": [],
"src_in_anchor": [["I_0:out0", "Dropout:in0"]],
"src_out_tensor": ["Dropout:out0", "Dropout:out1"],
"acu_lys_alias": ["noop"],
"src_acu_in_tensor_map": [["I_0:out0", "noop:in0"]],
"src_acu_out_tensor_map": [["Dropout:out0", "noop:out0"]],
"acu_inter_flow": [],
"param_map": {"noop": {}},
"blob_map": {"noop": {}},
"priority_tip": 0,
"pre_condition": "math.isclose(0.0, self.attr_pick(node['Dropout'], 'ratio', 0.5)) == True",
"src_ops_main_version": None,
"src_ops_minior_version": [7, -1]}
ruler_list.append(r_dropout_out2_as_noop_7)

r_dropout_out1_as_dropout_7 = {
"ruler_name": "r_dropout_out1_as_dropout_7",
"src_ops_alias": ["Dropout"],
"src_inter_flow": [],
"src_in_anchor": [["I:out0", "Dropout:in0"]],
"src_out_tensor": ["Dropout:out0"],
"acu_lys_alias": ["dropout"],
"src_acu_in_tensor_map": [["I:out0", "dropout:in0"]],
"src_acu_out_tensor_map": [["Dropout:out0", "dropout:out0"]],
"param_map": {'dropout':{'ratio':['FLOAT', 'CODE', "self.attr_pick(node['Dropout'], 'ratio', 0.5)"]}},
"blob_map": {},
"acu_inter_flow": [],
"priority_tip": 0,
"pre_condition": "math.isclose(0.0, self.attr_pick(node['Dropout'], 'ratio', 0.5)) == False",
"src_ops_main_version": None,
"src_ops_minior_version": [7, -1]}
ruler_list.append(r_dropout_out1_as_dropout_7)

r_dropout_out1_as_noop_7 = {
"ruler_name": "r_dropout_out1_as_noop_7",
"src_ops_alias": ["Dropout"],
"src_inter_flow": [],
"src_in_anchor": [["I:out0", "Dropout:in0"]],
"src_out_tensor": ["Dropout:out0"],
"acu_lys_alias": ["noop"],
"src_acu_in_tensor_map": [["I:out0", "noop:in0"]],
"src_acu_out_tensor_map": [["Dropout:out0", "noop:out0"]],
"param_map": {},
"blob_map": {},
"acu_inter_flow": [],
"priority_tip": 0,
"pre_condition": "math.isclose(0.0, self.attr_pick(node['Dropout'], 'ratio', 0.5)) == True",
"src_ops_main_version": None,
"src_ops_minior_version": [7, -1]}
ruler_list.append(r_dropout_out1_as_noop_7)

r_lrn = {
"ruler_name": "r_lrn",
"src_ops_alias": ["LRN"],
"src_inter_flow": [],
"src_in_anchor": [["I:out0", "LRN:in0"]],
"src_out_tensor": ["LRN:out0"],
"acu_lys_alias": ["localresponsenormalization"],
"src_acu_in_tensor_map": [["I:out0", "localresponsenormalization:in0"]],
"src_acu_out_tensor_map": [["LRN:out0", "localresponsenormalization:out0"]],
"param_map":
    {"localresponsenormalization":
         {"beta": ["FLOAT", "CODE", "self.attr_pick(node['LRN'], 'beta', 0.75)"],
          "type": ["STRING", "VALUE", "NORM_ACROSS_CHANNELS"],
          "local_size": ["INT", "CODE", "self.attr_pick(node['LRN'], 'size')"],
          "alpha": ["FLOAT", "CODE", "self.attr_pick(node['LRN'], 'alpha', 1e-4)"],
          "bias": ["FLOAT", "CODE", "self.attr_pick(node['LRN'], 'bias', 1.0)"]}},
"blob_map": {},
"acu_inter_flow": [],
"priority_tip": 0,
"pre_condition": None,
"src_ops_main_version": None,
"src_ops_minior_version": [1, -1]}
ruler_list.append(r_lrn)

r_reducel2_div_2_l2normalize = {
"ruler_name": "r_reducel2_div_2_l2normalize",
"src_ops_alias": ["Div", "ReduceL2"],
"src_inter_flow": [["ReduceL2:out0", "Div:in1"]],
"src_in_anchor": [["I_0:out0", "Div:in0"], ["I_0:out0", "ReduceL2:in0"]],
"src_out_tensor": ["Div:out0"],
"acu_lys_alias": ["l2normalizescale"],
"src_acu_in_tensor_map": [["I_0:out0", "l2normalizescale:in0"]],
"src_acu_out_tensor_map": [["Div:out0", "l2normalizescale:out0"]],
"acu_inter_flow": [],
"param_map": {
    "l2normalizescale":
        {'l2n_dim': ['INTS','CODE', "self.attr_pick(node['ReduceL2'], 'axes')"]}},
"blob_map": {"l2normalizescale": {}},
"priority_tip": 0,
"pre_condition": "self.attr_pick(node['ReduceL2'], 'keepdims') == 1",
"src_ops_main_version": None,
"src_ops_minior_version": [1, -1]}
ruler_list.append(r_reducel2_div_2_l2normalize)

r_l2normalaize_scale = {
"ruler_name": "l2normalaize_scale",
"src_ops_alias": ["Mul", "Div", "Reshape", "Add", "Constant", "Constant_1",
                  "Sqrt", "Constant_2", "ReduceSum", "Pow", "Constant_3"],
"src_inter_flow": [["Div:out0", "Mul:in0"], ["Reshape:out0", "Mul:in1"],
                   ["Add:out0", "Div:in1"], ["Constant:out0", "Reshape:in0"], ["Constant_1:out0", "Reshape:in1"],
                   ["Sqrt:out0", "Add:in0"], ["Constant_2:out0", "Add:in1"], ["ReduceSum:out0", "Sqrt:in0"],
                   ["Pow:out0", "ReduceSum:in0"], ["Constant_3:out0", "Pow:in1"]],
"src_in_anchor": [["I_0:out0", "Div:in0"], ["I_0:out0", "Pow:in0"]],
"src_out_tensor": ["Mul:out0"],
"acu_lys_alias": ["l2normalizescale"],
"src_acu_in_tensor_map": [["I_0:out0", "l2normalizescale:in0"]],
"src_acu_out_tensor_map": [["Mul:out0", "l2normalizescale:out0"]],
"acu_inter_flow": [],
"param_map": {"l2normalizescale": {'l2n_dim': ['ORIGIN','VALUE', -1]}},
"blob_map":
    {"l2normalizescale":
         {'scale':
              ['CODE',
               "np.reshape("\
               "self.tensor_to_numpy(tensor['Constant:out0']), "\
               "self.array_layout("\
               "self.tensor_to_numpy(tensor['Constant_1:out0']).astype(np.int32).tolist(),"\
               " [0, 2, 3, 1]))"],}},
"priority_tip": 0,
"pre_condition": "(self.tensor_to_numpy(tensor['Constant_3:out0']) == 2.0).all() and "\
                 " self.attr_pick(node['ReduceSum'], 'axes') == [1]",
"src_ops_main_version": None,
"src_ops_minior_version": [5, -1]}
ruler_list.append(r_l2normalaize_scale)

r_instancenorm = {
"ruler_name": "r_instancenorm",
"src_ops_alias": ["InstanceNormalization", "Constant_0", "Constant_1"],
"src_inter_flow": [["Constant_0:out0", "InstanceNormalization:in1"],
                   ["Constant_1:out0", "InstanceNormalization:in2"]],
"src_in_anchor": [["I:out0", "InstanceNormalization:in0"]],
"src_out_tensor": ["InstanceNormalization:out0"],
"acu_lys_alias": ["instancenormalize"],
"src_acu_in_tensor_map": [["I:out0", "instancenormalize:in0"]],
"src_acu_out_tensor_map": [["InstanceNormalization:out0", "instancenormalize:out0"]],
"param_map":
    {"instancenormalize":
         {'eps': ['FLOAT', 'CODE', "self.attr_pick(node['InstanceNormalization'], 'epsilon', 1e-5)"],
          'axis':
              ['INTS', 'CODE', "list(range(2, len(self.attr_pick(node['InstanceNormalization'], '_out_shape'))))"]
          }
     },
"blob_map": {"instancenormalize": {'bias': ['CODE', "self.tensor_to_numpy(tensor['Constant_1:out0'])"],
                                'scale': ['CODE', "self.tensor_to_numpy(tensor['Constant_0:out0'])"],}},
"acu_inter_flow": [],
"priority_tip": 0,
"pre_condition": None,
"src_ops_main_version": None,
"src_ops_minior_version": [6, -1]}
ruler_list.append(r_instancenorm)

r_add = {
"ruler_name": "r_add",
"src_ops_alias": ["Add"],
"src_inter_flow": [],
"src_in_anchor": [["I:out0", "Add:in0"], ['I_1:out0', "Add:in1"]],
"src_out_tensor": ["Add:out0"],
"acu_lys_alias": ["add"],
"src_acu_in_tensor_map":[["I:out0", "add:in0"], ['I_1:out0', "add:in1"]],
"src_acu_out_tensor_map": [["Add:out0", "add:out0"]],
"param_map": {},
"blob_map": {},
"acu_inter_flow": [],
"priority_tip": 0,
"pre_condition": None,
"src_ops_main_version": None,
"src_ops_minior_version": [1, -1]}
ruler_list.append(r_add)

r_sub = {
"ruler_name": "r_sub",
"src_ops_alias": ["Sub"],
"src_inter_flow": [],
"src_in_anchor": [["I:out0", "Sub:in0"], ['I_1:out0', "Sub:in1"]],
"src_out_tensor": ["Sub:out0"],
"acu_lys_alias": ["subtract"],
"src_acu_in_tensor_map":[["I:out0", "subtract:in0"], ['I_1:out0', "subtract:in1"]],
"src_acu_out_tensor_map": [["Sub:out0", "subtract:out0"]],
"param_map": {},
"blob_map": {},
"acu_inter_flow": [],
"priority_tip": 0,
"pre_condition": None,
"src_ops_main_version": None,
"src_ops_minior_version": [1, -1]}
ruler_list.append(r_sub)

r_mul = {
"ruler_name": "r_mul",
"src_ops_alias": ["Mul"],
"src_inter_flow": [],
"src_in_anchor": [["I:out0", "Mul:in0"], ['I_1:out0', "Mul:in1"]],
"src_out_tensor": ["Mul:out0"],
"acu_lys_alias": ["multiply"],
"src_acu_in_tensor_map":[["I:out0", "multiply:in0"], ['I_1:out0', "multiply:in1"]],
"src_acu_out_tensor_map": [["Mul:out0", "multiply:out0"]],
"param_map": {},
"blob_map": {},
"acu_inter_flow": [],
"priority_tip": 0,
"pre_condition": None,
"src_ops_main_version": None,
"src_ops_minior_version": [1, -1]}
ruler_list.append(r_mul)

r_div = {
"ruler_name": "r_div",
"src_ops_alias": ["Div"],
"src_inter_flow": [],
"src_in_anchor": [["I:out0", "Div:in0"], ['I_1:out0', "Div:in1"]],
"src_out_tensor": ["Div:out0"],
"acu_lys_alias": ["divide"],
"src_acu_in_tensor_map":[["I:out0", "divide:in0"], ['I_1:out0', "divide:in1"]],
"src_acu_out_tensor_map": [["Div:out0", "divide:out0"]],
"param_map": {},
"blob_map": {},
"acu_inter_flow": [],
"priority_tip": 0,
"pre_condition": None,
"src_ops_main_version": None,
"src_ops_minior_version": [1, -1]}
ruler_list.append(r_div)

r_abs = {
"ruler_name": "r_abs",
"src_ops_alias": ["Abs"],
"src_inter_flow": [],
"src_in_anchor": [["I:out0", "Abs:in0"]],
"src_out_tensor": ["Abs:out0"],
"acu_lys_alias": ["abs"],
"src_acu_in_tensor_map": [["I:out0", "abs:in0"]],
"src_acu_out_tensor_map": [["Abs:out0", "abs:out0"]],
"param_map": {},
"blob_map": {},
"acu_inter_flow": [],
"priority_tip": 0,
"pre_condition": None,
"src_ops_main_version": None,
"src_ops_minior_version": [1, -1]}
ruler_list.append(r_abs)

r_floor = {
"ruler_name": "r_floor",
"src_ops_alias": ["Floor"],
"src_inter_flow": [],
"src_in_anchor": [["I:out0", "Floor:in0"]],
"src_out_tensor": ["Floor:out0"],
"acu_lys_alias": ["floor"],
"src_acu_in_tensor_map": [["I:out0", "floor:in0"]],
"src_acu_out_tensor_map": [["Floor:out0", "floor:out0"]],
"param_map": {},
"blob_map": {},
"acu_inter_flow": [],
"priority_tip": 0,
"pre_condition": None,
"src_ops_main_version": None,
"src_ops_minior_version": [1, -1]}
ruler_list.append(r_floor)

r_sqrt = {
"ruler_name": "r_sqrt",
"src_ops_alias": ["Sqrt"],
"src_inter_flow": [],
"src_in_anchor": [["I:out0", "Sqrt:in0"]],
"src_out_tensor": ["Sqrt:out0"],
"acu_lys_alias": ["sqrt"],
"src_acu_in_tensor_map": [["I:out0", "sqrt:in0"]],
"src_acu_out_tensor_map": [["Sqrt:out0", "sqrt:out0"]],
"param_map": {},
"blob_map": {},
"acu_inter_flow": [],
"priority_tip": 0,
"pre_condition": None,
"src_ops_main_version": None,
"src_ops_minior_version": [1, -1]}
ruler_list.append(r_sqrt)

r_exp = {
"ruler_name": "r_exp",
"src_ops_alias": ["Exp"],
"src_inter_flow": [],
"src_in_anchor": [["I:out0", "Exp:in0"]],
"src_out_tensor": ["Exp:out0"],
"acu_lys_alias": ["exp"],
"src_acu_in_tensor_map": [["I:out0", "exp:in0"]],
"src_acu_out_tensor_map": [["Exp:out0", "exp:out0"]],
"param_map": {},
"blob_map": {},
"acu_inter_flow": [],
"priority_tip": 0,
"pre_condition": None,
"src_ops_main_version": None,
"src_ops_minior_version": [1, -1]}
ruler_list.append(r_exp)



r_reducesum = {
"ruler_name": "r_reducesum",
"src_ops_alias": ["ReduceSum"],
"src_inter_flow": [],
"src_in_anchor": [["I:out0", "ReduceSum:in0"]],
"src_out_tensor": ["ReduceSum:out0"],
"acu_lys_alias": ["reducesum"],
"src_acu_in_tensor_map": [["I:out0", "reducesum:in0"]],
"src_acu_out_tensor_map": [["ReduceSum:out0", "reducesum:out0"]],
"param_map": {'reducesum':{'axis_list': ['ORIGIN','CODE', "self.attr_pick(node['ReduceSum'], 'axes')"],
                           'keep_dims': ['BOOL','CODE', "self.attr_pick(node['ReduceSum'], 'keepdims')"]}},
"blob_map": {},
"acu_inter_flow": [],
"priority_tip": 0,
"pre_condition": None,
"src_ops_main_version": None,
"src_ops_minior_version": [1, -1]}
ruler_list.append(r_reducesum)


r_rsp_tsp_rsp_2_shuffle_v1 = {
"ruler_name": "r_rsp_tsp_rsp_2_shuffle_v1",
"src_ops_alias": ["Reshape", "Transpose", "Reshape_1"],
"src_inter_flow": [["Reshape:out0", "Transpose:in0"], ["Transpose:out0", "Reshape_1:in0"]],
"src_in_anchor": [["I:out0", "Reshape:in0"]],
"src_out_tensor": ["Reshape_1:out0"],
"acu_lys_alias": ["shuffle"],
"src_acu_in_tensor_map": [["I:out0", "shuffle:in0"]],
"src_acu_out_tensor_map": [["Reshape_1:out0", "shuffle:out0"]],
"param_map": {"shuffle": {"group_number": ["INT", "CODE", "self.attr_pick(node['Reshape'], 'shape')[1]"]}},
"blob_map": {},
"acu_inter_flow": [],
"priority_tip": 0,
"pre_condition": "len(self.attr_pick(node['Reshape'], 'shape')) == 5 and "\
    "len(self.attr_pick(node['Transpose'], 'perm')) == 5 and "\
    "len(self.attr_pick(node['Reshape_1'], 'shape')) == 4 and "\
    "self.attr_pick(node['Transpose'], 'perm') == [0, 2, 1, 3, 4]",
"src_ops_main_version": None,
"src_ops_minior_version": [1, 4]}
ruler_list.append(r_rsp_tsp_rsp_2_shuffle_v1)

r_rsp_tsp_rsp_2_shuffle_v5 = {
"ruler_name": "r_rsp_tsp_rsp_2_shuffle_v5",
"src_ops_alias": ["Reshape", "Transpose", "Reshape_1", "Constant_0"],
"src_inter_flow": [["Reshape:out0", "Transpose:in0"], ["Transpose:out0", "Reshape_1:in0"]],
"src_in_anchor": [["I:out0", "Reshape:in0"]],
"src_out_tensor": ["Reshape_1:out0"],
"acu_lys_alias": ["shuffle"],
"src_acu_in_tensor_map": [["I:out0", "shuffle:in0"]],
"src_acu_out_tensor_map": [["Reshape_1:out0", "shuffle:out0"]],
"param_map": {"shuffle": {"group_number": ["INT", "CODE", "self.tensor_to_numpy(tensor['Constant_0:out0'])[1]"]}},
"blob_map": {},
"acu_inter_flow": [],
"priority_tip": 0,
"pre_condition": "len(self.tensor_to_numpy(tensor['Reshape'].inputs[1])) == 5 and "\
    "len(self.attr_pick(node['Transpose'], 'perm')) == 5 and "\
    "len(self.tensor_to_numpy(tensor['Reshape_1'].inputs[1])) == 4 and "\
    "self.attr_pick(node['Transpose'], 'perm') == [0, 2, 1, 3, 4]",
"src_ops_main_version": None,
"src_ops_minior_version": [5, -1]}
ruler_list.append(r_rsp_tsp_rsp_2_shuffle_v5)

r_identity = {
"ruler_name": "r_identity",
"src_ops_alias": ["Identity"],
"src_inter_flow": [],
"src_in_anchor": [["I:out0", "Identity:in0"]],
"src_out_tensor": ["Identity:out0"],
"acu_lys_alias": ["noop"],
"src_acu_in_tensor_map": [["I:out0", "noop:in0"]],
"src_acu_out_tensor_map": [["Identity:out0", "noop:out0"]],
"param_map": {},
"blob_map": {},
"acu_inter_flow": [],
"priority_tip": 0,
"pre_condition": None,
"src_ops_main_version": None,
"src_ops_minior_version": [1, -1]}
ruler_list.append(r_identity)

r_constantfill = {
"ruler_name": "r_constantfill",
"src_ops_alias": ["ConstantFill"],
"src_inter_flow": [],
"src_in_anchor": [["I:out0", "ConstantFill:in0"]],
"src_out_tensor": ["ConstantFill:out0"],
"acu_lys_alias": ["noop"],
"src_acu_in_tensor_map": [["I:out0", "noop:in0"]],
"src_acu_out_tensor_map": [["ConstantFill:out0", "noop:out0"]],
"param_map": {},
"blob_map": {},
"acu_inter_flow": [],
"priority_tip": 0,
"pre_condition": None,
"src_ops_main_version": None,
"src_ops_minior_version": [1, -1]}
ruler_list.append(r_constantfill)

r_slice = {
"ruler_name": "r_slice",
"src_ops_alias": ["Slice"],
"src_inter_flow": [],
"src_in_anchor": [["I:out0", "Slice:in0"]],
"src_out_tensor": ["Slice:out0"],
"acu_lys_alias": ["slice"],
"src_acu_in_tensor_map": [["I:out0", "slice:in0"]],
"src_acu_out_tensor_map": [["Slice:out0", "slice:out0"]],
"param_map":
    {"slice":
         {"begin": ["INTS", "PYFUNC", r_slice_get_begin()],
          "size": ["INTS", "PYFUNC", r_slice_get_size()],
          }
     },
"blob_map": {},
"acu_inter_flow": [],
"priority_tip": 0,
"pre_condition": "self.attr_pick(node['Slice'], 'axes', None) == None",
"src_ops_main_version": None,
"src_ops_minior_version": [1, 9]}
ruler_list.append(r_slice)

r_slice_axes = {
"ruler_name": "r_slice_axes",
"src_ops_alias": ["Slice"],
"src_inter_flow": [],
"src_in_anchor": [["I:out0", "Slice:in0"]],
"src_out_tensor": ["Slice:out0"],
"acu_lys_alias": ["slice"],
"src_acu_in_tensor_map": [["I:out0", "slice:in0"]],
"src_acu_out_tensor_map": [["Slice:out0", "slice:out0"]],
"param_map":
    {"slice":
         {"begin": ["INTS", "PYFUNC", r_slice_get_begin()],
          "size": ["INTS", "PYFUNC", r_slice_get_size()],
          }
     },
"blob_map": {},
"acu_inter_flow": [],
"priority_tip": 0,
"pre_condition": "self.attr_pick(node['Slice'], 'axes', None) != None",
"src_ops_main_version": None,
"src_ops_minior_version": [1, 9]}
ruler_list.append(r_slice_axes)


r_upsample_l7_to_resize = {
"ruler_name": "upsample_l8_to_resize",
"src_ops_alias": ["Upsample"],
"src_inter_flow": [],
"src_in_anchor": [["I:out0", "Upsample:in0"]],
"src_out_tensor": ["Upsample:out0"],
"acu_lys_alias": ["image_resize"],
"src_acu_in_tensor_map": [["I:out0", "image_resize:in0"]],
"src_acu_out_tensor_map": [["Upsample:out0", "image_resize:out0"]],
"param_map": {"image_resize":
                  {"new_size": ["ORIGIN", "CODE", "self.shape_pick(tensor['Upsample:out0'])[2:]"],
                    "align_corners": ["BOOL", "VALUE", False],
                    "type": ['STRING', 'CODE', "self.attr_pick(node['Upsample'], 'mode')"],}
              },
"blob_map": {"image_resize": {}},
"acu_inter_flow": [],
"priority_tip": 0,
"pre_condition": None,
"src_ops_main_version": None,
"src_ops_minior_version": [1, 6]}
ruler_list.append(r_upsample_l7_to_resize)

r_upsample_l9_to_resize = {
"ruler_name": "upsample_l9_to_resize",
"src_ops_alias": ["Upsample", "Constant"],
"src_inter_flow": [["Constant:out0", "Upsample:in1"]],
"src_in_anchor": [["I:out0", "Upsample:in0"]],
"src_out_tensor": ["Upsample:out0"],
"acu_lys_alias": ["image_resize"],
"src_acu_in_tensor_map": [["I:out0", "image_resize:in0"]],
"src_acu_out_tensor_map": [["Upsample:out0", "image_resize:out0"]],
"param_map": {"image_resize":
                  {"new_size": ["ORIGIN", "CODE", "self.shape_pick(tensor['Upsample:out0'])[2:]"],
                    "align_corners": ["BOOL", "VALUE", False],
                    "type":
                       ['STRING',
                        'CODE',
                        "'bilinear' "
                        "if self.attr_pick(node['Upsample'], 'mode') == 'linear' "
                        "else self.attr_pick(node['Upsample'], 'mode')"],}
              },
"blob_map": {"image_resize": {}},
"acu_inter_flow": [],
"priority_tip": 11,
"pre_condition": None,
"src_ops_main_version": None,
"src_ops_minior_version": [7, 8]}
ruler_list.append(r_upsample_l9_to_resize)

r_upsample_l9_scale_to_resize = {
"ruler_name": "r_upsample_l9_scale_to_resize",
"src_ops_alias": ["Upsample", ],
"src_inter_flow": [],
"src_in_anchor": [["I:out0", "Upsample:in0"], ["I_1:out0", "Upsample:in1"]],
"src_out_tensor": ["Upsample:out0"],
"acu_lys_alias": ["image_resize"],
"src_acu_in_tensor_map": [["I:out0", "image_resize:in0"]],
"src_acu_out_tensor_map": [["Upsample:out0", "image_resize:out0"]],
"param_map": {"image_resize":
                  {"new_size": ["ORIGIN", "CODE", "self.shape_pick(tensor['Upsample:out0'])[2:]"],
                    "align_corners": ["BOOL", "VALUE", False],
                    "type":
                       ['STRING',
                        'CODE',
                        "'bilinear' "
                        "if self.attr_pick(node['Upsample'], 'mode') == 'linear' "
                        "else self.attr_pick(node['Upsample'], 'mode')"],}
              },
"blob_map": {"image_resize": {}},
"acu_inter_flow": [],
"priority_tip": 0,
"pre_condition": None,
"src_ops_main_version": None,
"src_ops_minior_version": [9, -1]}
ruler_list.append(r_upsample_l9_scale_to_resize)


r_pad_g1 = {
"ruler_name": "pad_g1",
"src_ops_alias": ["Pad"],
"src_inter_flow": [],
"src_in_anchor": [["I:out0", "Pad:in0"]],
"src_out_tensor": ["Pad:out0"],
"acu_lys_alias": ["pad"],
"src_acu_in_tensor_map": [["I:out0", "pad:in0"]],
"src_acu_out_tensor_map": [["Pad:out0", "pad:out0"]],
"param_map": {"pad":
                  {'padding_value': ['ORIGIN', 'CODE', "self.map_pad_value(node['Pad'])"],
                   'padding_mode': ['STRING', 'CODE', "self.attr_pick(node['Pad'], 'mode')"],
                   'padding_const': ['ORIGIN', 'CODE', "self.attr_pick(node['Pad'], 'value')"],
                   }
              },
"blob_map": {"pad": {}},
"acu_inter_flow": [],
"priority_tip": 0,
"pre_condition": None,
"src_ops_main_version": None,
"src_ops_minior_version": [2, 10]}
ruler_list.append(r_pad_g1)

r_pad_11 = {
"ruler_name": "r_pad_11",
"src_ops_alias": ["Pad", "Constant"],
"src_inter_flow": [["Constant:out0", "Pad:in1"]],
"src_in_anchor": [["I:out0", "Pad:in0"]],
"src_out_tensor": ["Pad:out0"],
"acu_lys_alias": ["pad"],
"src_acu_in_tensor_map": [["I:out0", "pad:in0"]],
"src_acu_out_tensor_map": [["Pad:out0", "pad:out0"]],
"param_map": {"pad":
                  {'padding_value': ['ORIGIN', 'PYFUNC', r_pad_value_map()],
                   'padding_mode': ['STRING', 'CODE', "self.attr_pick(node['Pad'], 'mode')"],
                   }
              },
"blob_map": {"pad": {}},
"acu_inter_flow": [],
"priority_tip": 0,
"pre_condition": None,
"src_ops_main_version": None,
"src_ops_minior_version": [11, -1]}
ruler_list.append(r_pad_11)

r_space2depth = {
"ruler_name": "r_space2depth",
"src_ops_alias": ["SpaceToDetph"],
"src_inter_flow": [],
"src_in_anchor": [["I:out0", "SpaceToDetph:in0"]],
"src_out_tensor": ["SpaceToDetph:out0"],
"acu_lys_alias": ["space2depth"],
"src_acu_in_tensor_map": [["I:out0", "space2depth:in0"]],
"src_acu_out_tensor_map": [["SpaceToDetph:out0", "space2depth:out0"]],
"param_map":
    {"space2depth":
         {"block_size":
["INTS",
 "CODE",
 "[self.attr_pick(node['SpaceToDetph'], 'blocksize'), self.attr_pick(node['SpaceToDetph'], 'blocksize')]"]
          }
     },
"blob_map": {},
"acu_inter_flow": [],
"priority_tip": 0,
"pre_condition": None,
"src_ops_main_version": None,
"src_ops_minior_version": [1, -1]}
ruler_list.append(r_space2depth)

r_depth2space = {
"ruler_name": "r_depth2space",
"src_ops_alias": ["DepthToSpace"],
"src_inter_flow": [],
"src_in_anchor": [["I:out0", "DepthToSpace:in0"]],
"src_out_tensor": ["DepthToSpace:out0"],
"acu_lys_alias": ["depth2space"],
"src_acu_in_tensor_map": [["I:out0", "depth2space:in0"]],
"src_acu_out_tensor_map": [["DepthToSpace", "depth2space:out0"]],
"param_map":
{"depth2space":
     {"block_size":
          ["INTS",
           "CODE",
           "[self.attr_pick(node['DepthToSpace'], 'blocksize'), self.attr_pick(node['DepthToSpace'], 'blocksize')]"
           ]
      }
 },
"blob_map": {},
"acu_inter_flow": [],
"priority_tip": 0,
"pre_condition": None,
"src_ops_main_version": None,
"src_ops_minior_version": [1, -1]}
ruler_list.append(r_depth2space)

r_clip = {
"ruler_name": "r_clip",
"src_ops_alias": ["Clip"],
"src_inter_flow": [],
"src_in_anchor": [["I_0:out0", "Clip:in0"]],
"src_out_tensor": ["Clip:out0"],
"acu_lys_alias": ["clipbyvalue"],
"src_acu_in_tensor_map": [["I_0:out0", "clipbyvalue:in0"]],
"src_acu_out_tensor_map": [["Clip:out0", "clipbyvalue:out0"]],
"acu_inter_flow": [],
"param_map": {'clipbyvalue':
                  {'clip_value_max': ['FLOAT', 'CODE',
                                      "self.attr_pick(node['Clip'], 'max', 1.0)"],
                  'clip_value_min': ['FLOAT', 'CODE',
                                        "self.attr_pick(node['Clip'], 'min', -1.0)"]}},
"blob_map": {"clipbyvalue": {}},
"priority_tip": 0,
"pre_condition": None,
"src_ops_main_version": None,
"src_ops_minior_version": [1, -1]}
ruler_list.append(r_clip)

r_reducemean = {
"ruler_name": "r_reducemean",
"src_ops_alias": ["ReduceMean"],
"src_inter_flow": [],
"src_in_anchor": [["I_0:out0", "ReduceMean:in0"]],
"src_out_tensor": ["ReduceMean:out0"],
"acu_lys_alias": ["reducemean"],
"src_acu_in_tensor_map": [["I_0:out0", "reducemean:in0"]],
"src_acu_out_tensor_map": [["ReduceMean:out0", "reducemean:out0"]],
"acu_inter_flow": [],
"param_map": {'reducemean':{'axis_list': ['ORIGIN','CODE', "self.attr_pick(node['ReduceMean'], 'axes')"],
                           'keep_dims': ['BOOL','CODE', "self.attr_pick(node['ReduceMean'], 'keepdims')"]}},
"blob_map": {"reducemean": {}},
"priority_tip": 0,
"pre_condition": None,
"src_ops_main_version": None,
"src_ops_minior_version": [1, -1]}
ruler_list.append(r_reducemean)

r_gather = {
"ruler_name": "gather",
"src_ops_alias": ["Gather"],
"src_inter_flow": [],
"src_in_anchor": [["I:out0", "Gather:in0"], ["I_1:out0", "Gather:in1"]],
"src_out_tensor": ["Gather:out0"],
"acu_lys_alias": ["gather"],
"src_acu_in_tensor_map": [["I:out0", "gather:in0"], ["I_1:out0", "gather:in1"]],
"src_acu_out_tensor_map": [["Gather:out0", "gather:out0"]],
"acu_inter_flow": [],
"param_map": {"gather": {'axis': ['INT', 'CODE', "self.attr_pick(node['Gather'], 'axis')"]}},
"blob_map": {"gather": {}},
"priority_tip": 0,
"pre_condition": None}
ruler_list.append(r_gather)

r_matmul = {
"ruler_name": "r_matmul",
"src_ops_alias": ["MatMul"],
"src_inter_flow": [],
"src_in_anchor": [["I:out0", "MatMul:in0"], ['I_1:out0', "MatMul:in1"]],
"src_out_tensor": ["MatMul:out0"],
"acu_lys_alias": ["matmul"],
"src_acu_in_tensor_map":[["I:out0", "matmul:in0"], ['I_1:out0', "matmul:in1"]],
"src_acu_out_tensor_map": [["MatMul:out0", "matmul:out0"]],
"param_map": {"matmul": {'transpose_a': ['BOOL', 'CODE', "self.attr_pick(node['MatMul'], 'transpose_a', False)"],
                         'transpose_b': ['BOOL', 'CODE', "self.attr_pick(node['MatMul'], 'transpose_b', False)"],}},
"blob_map": {},
"acu_inter_flow": [],
"priority_tip": 0,
"pre_condition": None,
"src_ops_main_version": None,
"src_ops_minior_version": [1, -1]}
ruler_list.append(r_matmul)

def gen_onnx_ruler(dst_path):
    # print(json.dumps(ruler_list))
    dst_path = os.path.join(dst_path, 'onnx_ruler_db.json')

    with open(dst_path, 'w+') as f:
        json.dump(ruler_list, f, indent=1)

    # To Verify ruler follow synatx
    with open(dst_path, 'r') as f:
        x_val = json.load(f)
def main():
    gen_onnx_ruler(sys.argv[1])

if  __name__ == '__main__':
    main()

