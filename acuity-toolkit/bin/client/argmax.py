import copy
from acuitylib.layer.customlayer import CustomLayer
from acuitylib.layer.acuitylayer import IoMap
from acuitylib.core.shape import Shape
from acuitylib.utils import convert_axis_if_need
from acuitylib.xtf import xtf as tf
import numpy as np

class ArgMax(CustomLayer):

    op = 'argmax'

    # label, description
    def_input  = [IoMap('in0', 'in', 'input port')]
    def_output = [IoMap('out0', 'out', 'output port')]

    def setup(self, inputs, outputs):
        in_shape = inputs[0].shape.format('nhwc')
        axis = convert_axis_if_need(self.net.get_platform(),
                self.params.axis, inputs[0].shape.length())
        out_shape = copy.deepcopy(in_shape)
        out_shape.remove(in_shape[axis])
        outputs[0].shape = Shape(shape = out_shape, fmt = 'nhwc')

    def load_params_from_caffe(self, cl):
        p = dict()
        p['axis'] = cl.argmax_param.axis
        self.set_params(p)

    def load_params_from_tf(self, ruler, layer_alias, op_alias_map, tensor_data_map, anet=None):
        p = dict()
        p['axis'] = int(tensor_data_map['C:out0'])
        self.set_params(p)

    def compute_out_tensor(self, tensor, input_tensor, ac_sess):
        axis = convert_axis_if_need(self.net.get_platform(),
                self.params.axis, self.get_in_shape().length())
        out = tf.argmax(input_tensor[0], axis)
        return [out]
