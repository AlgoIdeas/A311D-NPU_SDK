from acuitylib.layer.customlayer import CustomLayer
from acuitylib.layer.acuitylayer import IoMap
from acuitylib.core.shape import Shape
from acuitylib.xtf import xtf as tf

class Maximum(CustomLayer):

    op = 'maximum'

    # label, description
    def_input  = [IoMap('in0', 'in', 'input port'), IoMap('in1', 'in', 'input port')]
    def_output = [IoMap('out0', 'out', 'output port')]


    def setup(self, inputs, outputs):
        in_shape0 = inputs[0].shape
        in_shape1 = inputs[1].shape
        if in_shape0.length() > in_shape1.length():
            out_shape = in_shape0.copy()
        else:
            out_shape = in_shape1.copy()

        outputs[0].shape = out_shape

    def compute_out_tensor(self, tensor, input_tensor, ac_sess):
        out = tf.maximum(input_tensor[0], input_tensor[1])
        return [out]