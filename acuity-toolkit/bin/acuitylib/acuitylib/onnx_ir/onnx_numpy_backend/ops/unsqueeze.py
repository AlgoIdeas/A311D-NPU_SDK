from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import numpy as np  # type: ignore


def Unsqueeze(inputs, outputs, attr=None, op_version=11):  # type: (np.ndarray) -> np.ndarray
    res = inputs[0]
    for axis in sorted(attr['axes']):
      res = np.expand_dims(res, axis=axis)
    return res
