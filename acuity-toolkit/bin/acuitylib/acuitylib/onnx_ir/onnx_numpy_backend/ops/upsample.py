from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals
from acuitylib.acuitylog import AcuityLog as al

import numpy as np  # type: ignore

def nearest(data, new_shape, scales):
    new_data = np.ones(new_shape, data.dtype)
    for location in np.ndindex(tuple(new_shape)):
        orig_local = location / scales
        max_at = list([s-1 for s in data.shape])
        orig_local = np.minimum(np.round(orig_local).astype(np.int64), max_at)
        new_data[location] = data[tuple(orig_local)]
    return new_data

def bilinear(data, new_shape, scales):
    new_data = np.ones(new_shape, data.dtype)
    al.w('Miss bilinear implement of Upsample')
    return new_data

def trilinear(data, new_shape, scales):
    new_data = np.ones(new_shape, data.dtype)
    al.w('Miss trilinear implement of Upsample')
    return new_data

def Upsample(inputs, outputs, attr=None, op_version=11):  # type: (np.ndarray) -> np.ndarray
    data = inputs[0]
    mode = attr.get('mode', 'nearest')
    if op_version <= 7:
        scale = attr['scales']
    else:
        scale = inputs[1]

    # This is a correction value to fix the accuracy issue of float64 to float32
    # eg: out = 35.0, in = 18.0, scale = 35.0 / 18.0 = 1.94444444
    #     but out = floor(1.9444444 * 18.9) = 34
    correction_value = 1e-5
    new_shape = np.floor(np.multiply(data.shape, scale) + correction_value).astype(np.int32)
    if 'infer_shape' in attr:
        return np.ones(new_shape, data.dtype)

    if mode == 'nearest':
        return nearest(data, new_shape, scale)
    elif mode in ['linear', 'bilinear']:
        return bilinear(data, new_shape, scale)
    elif mode == 'trilinear':
        return trilinear(data, new_shape, scale)
    else:
        al.e('Unsupport Upsample mode {}'.format(mode))
