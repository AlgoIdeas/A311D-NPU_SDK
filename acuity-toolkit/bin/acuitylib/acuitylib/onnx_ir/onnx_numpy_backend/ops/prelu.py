from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import numpy as np  # type: ignore


def PRelu(inputs, outputs, attr=None, op_version=11):  # type: (np.ndarray) -> np.ndarray
    x = inputs[0]
    slope = inputs[1]
    if slope.ndim == 1 and slope.shape[0] != 1:
        # slope can unidirectional broadcastable to x
        ss = slope.shape[0]
        shape_index = -1 if ss not in list(x.shape[0:2]) else list(x.shape[0:2]).index(ss)
        if shape_index == -1:
            shape_index = -1 if ss not in list(reversed(x.shape[2:])) else list(reversed(x.shape[2:])).index(ss)
            assert(shape_index != -1)
            shape_index = x.ndim - 2 - shape_index + 2
        shape = list()
        for i in range(x.ndim):
            if i != shape_index:
                shape.append(1)
            else:
                shape.append(slope.shape[0])
        slope = np.reshape(slope, shape)

    return np.clip(x, 0, np.inf) + np.clip(x, -np.inf, 0) * slope

