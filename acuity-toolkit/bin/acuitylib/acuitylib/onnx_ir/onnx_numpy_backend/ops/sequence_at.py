from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import numpy as np  # type: ignore


def SequenceAt(inputs, outputs, attr=None, op_version=11):  # type: (np.ndarray) -> np.ndarray
    data = inputs[0]
    pos = inputs[1]
    return data[pos]
