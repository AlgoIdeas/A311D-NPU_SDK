from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import numpy as np  # type: ignore


def Clip(inputs, outputs, attr=None, op_version=11):  # type: (np.ndarray) -> np.ndarray
    if op_version <= 6:
        min = attr.get('min', -np.inf)
        max = attr.get('max', np.inf)
    else:
        l = len(inputs)
        min = -np.inf if l < 2 or inputs[1] == '' else inputs[1]
        max = np.inf if l < 3 or inputs[2] == '' else inputs[2]

    return np.clip(inputs[0], min, max)
