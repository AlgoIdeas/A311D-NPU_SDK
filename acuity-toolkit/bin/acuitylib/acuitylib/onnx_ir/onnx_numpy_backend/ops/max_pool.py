from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import numpy as np  # type: ignore

import itertools
from acuitylib.xtf import xtf as tf


def get_pad_shape(auto_pad,  # type: str
                  input_spatial_shape,  # type: list[int]
                  kernel_spatial_shape,  # type: list[int]
                  strides_spatial,  # type: list[int]
                  dilations_spatial,  # type: list[int]
                  output_spatial_shape,  # type: list[int]
                  spatial_pad, #type: list[int]
                  ):  # type: (...) -> list[int]
    spatial_rank = len(input_spatial_shape)
    pad_shape = [[0, 0]] * spatial_rank
    if auto_pad in ('SAME_UPPER', 'SAME_LOWER'):
        for i in range(spatial_rank):
            pad_size = (output_spatial_shape[i] - 1) * strides_spatial[i] + (
                        (kernel_spatial_shape[i] - 1) * dilations_spatial[i] + 1) - input_spatial_shape[i]
            pad_shape[i][0] = pad_size // 2 if auto_pad == 'SAME_UPPER' else pad_size - pad_size // 2
            pad_shape[i][1] = pad_size - pad_size // 2 if auto_pad == 'SAME_UPPER' else pad_size // 2
    else:
        for i in range(spatial_rank):
            pad_shape[i][0] = spatial_pad[i]
            pad_shape[i][1] = spatial_pad[i + spatial_rank]
    return pad_shape


def get_output_shape(auto_pad,  # type: str
                     input_spatial_shape,  # type: list[int]
                     kernel_spatial_shape,  # type: list[int]
                     strides_spatial,  # type: list[int]
                     dilations,  # type: list[int]
                     spatial_pad, # type: list[int]
                     ceil_mode=False, # type:bool
                     ):  # type: (...) -> list[int]
    out_shape = [0] * len(input_spatial_shape)
    round_func = np.ceil if ceil_mode else np.floor
    spatial_rank = len(input_spatial_shape)
    if auto_pad in ('SAME_UPPER', 'SAME_LOWER'):
        for i in range(len(input_spatial_shape)):
            out_shape[i] = int(np.ceil(float(input_spatial_shape[i]) / float(strides_spatial[i])))
    elif auto_pad in ('VALID', 'NOTSET'):
        for i in range(spatial_rank):
            # out_shape[i] = int(round_func(float(
            #     input_spatial_shape[i] + spatial_pad[i] + spatial_pad[i + spatial_rank] - (
            #                 kernel_spatial_shape[i] - 1) * dilations[i] + 1) / float(strides_spatial[i]) + 1))
            out_shape[i] = int(round_func(float(
                input_spatial_shape[i] + spatial_pad[i] + spatial_pad[i + spatial_rank] - kernel_spatial_shape[i]) / float(strides_spatial[i])) + 1)
            if (out_shape[i] - 1) * strides_spatial[i] >= input_spatial_shape[i] + spatial_pad[i]:
                out_shape[i] = out_shape[i] - 1
    return out_shape

def pool(x,  # type: np.ndarray
         x_shape,  # type: Sequence[int]
         kernel_shape,  # type: Sequence[int]
         strides_shape,  # type: Sequence[int]
         out_shape,  # type: Sequence[int]
         pad_shape,  # type: Sequence[int]
         pooling_type,  # type: Text
         count_include_pad=0,  # type: int
         ):  # type: (...) -> np.ndarray
    spatial_size = len(x_shape) - 2
    y = np.zeros([x_shape[0], x_shape[1]] + list(out_shape))

    if pooling_type == 'AVG':
        f = np.average
    elif pooling_type == 'MAX':
        f = np.max
    elif pooling_type.startswith('LP'):
        p = int(pooling_type.replace('LP', ''))
        axis = [i for i in range(2, 2+spatial_size)]
        f = lambda x: np.linalg.norm(x=x, ord=p, axis=axis)
    else:
        raise NotImplementedError(
            'Pooling type {} does not support. Should be AVG, MAX'.format(pooling_type))

    for location in np.ndindex(y.shape):
        n,c = location[0], location[1]
        spatial_index = location[2:]
        index_arg = [n, c]
        for s_r in range(spatial_size):
            l = spatial_index[s_r]
            if pooling_type == 'MAX':
                start = l * strides_shape[s_r] - pad_shape[s_r][0]
                end = min(start + kernel_shape[s_r], x_shape[s_r + 2])
            elif pooling_type in ['AVG', 'LP']:
                if count_include_pad == 0:
                    start = l * strides_shape[s_r] - pad_shape[s_r][0]
                    end = min(start + kernel_shape[s_r], x_shape[s_r + 2] + pad_shape[s_r][1])
                    end = min(end, x_shape[s_r + 2])
                else:
                    start = l * strides_shape[s_r]
                    end = min(start + kernel_shape[s_r], x_shape[s_r + 2] + pad_shape[s_r][0] + pad_shape[s_r][1])
                    # end = min(end, x_shape[s_r + 2])
            start = max(start, 0)
            if start != end:
                index_arg.append((start, end))
            else:
                index_arg.append((start, start+1))
        if spatial_size == 1:
            to_process_data = x[n, c, index_arg[2][0]:index_arg[2][1]]
        elif spatial_size == 2:
            to_process_data = x[n, c, index_arg[2][0]:index_arg[2][1], index_arg[3][0]:index_arg[3][1]]
        else:
            index_arg_str = str(n) + ',' + str(c) + ',' + ','.join('{}:{}'.format(s,e) for s,e in index_arg[2:] )
            to_process_data = eval('x[{}]'.format(index_arg_str))

        y[location] = f(to_process_data)

    return y.astype(np.float32)


def MaxPool(inputs, outputs, attr=None, op_version=11):  # type: (np.ndarray) -> np.ndarray
    x = inputs[0]
    x_shape = np.shape(x)
    spatial_rank = len(x_shape) - 2
    kernel_shape = attr['kernel_shape']
    strides = attr.get('strides', [1]*spatial_rank)
    dilations = attr.get('dilations', [1] * spatial_rank)
    ceil_mode = attr.get('ceil_mode', 0) == 1
    spatial_pad = attr.get('pads', [0]*2*spatial_rank)
    store_location = attr.get('storage_order', 0) == 1 and len(outputs) == 2
    # TODO: need handle this case.
    out_spatial_shape = get_output_shape(attr.get('auto_pad', 'VALID', ), x_shape[2:], kernel_shape, strides,
                                         dilations=dilations, ceil_mode=ceil_mode, spatial_pad=spatial_pad)
    spatial_pads = get_pad_shape(attr.get('auto_pad', 'VALID'), x_shape[2:], kernel_shape, strides, dilations,
                                 out_spatial_shape, spatial_pad)
    pads = [(0, 0), (0, 0)] + spatial_pads
    # padded = np.pad(x, pads, mode='constant', constant_values=np.nan)
    if 'infer_shape' in attr:
        shape = list(x_shape[0:2])
        shape.extend(out_spatial_shape)
        return np.ones(shape, x.dtype)

    res = pool(x, x_shape, kernel_shape, strides, out_spatial_shape, spatial_pads, 'MAX')
    if len(outputs) == 2 and outputs[1] == '':
        return (res, None)
    return res
