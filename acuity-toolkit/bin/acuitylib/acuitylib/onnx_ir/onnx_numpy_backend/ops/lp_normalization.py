from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import numpy as np  # type: ignore



def LpNormalization(inputs, outputs, attr=None, op_version=11):  # type: (np.ndarray) -> np.ndarray
    x = inputs[0]
    axis = attr['axis']
    return np.linalg.norm(x=x, ord=attr['p'], axis=axis)

