from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import numpy as np  # type: ignore


def Split(inputs, outputs, attr=None, op_version=11):  # type: (np.ndarray) -> np.ndarray
    x = inputs[0]
    indices_or_sections = attr.get('split', None)
    axis = attr.get('axis', 0)
    if indices_or_sections == None:
        indices_or_sections = len(outputs)
    elif isinstance(indices_or_sections, list):
        indices_or_sections= np.cumsum(indices_or_sections)
    return np.split(x, indices_or_sections=indices_or_sections, axis=axis)
