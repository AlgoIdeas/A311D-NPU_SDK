from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import numpy as np  # type: ignore
from acuitylib.xtf import xtf as tf

def get_output_shape(auto_pad,
              kernel_spatial_shape,
              input_spatial_shape,
              dilations,
              strides_spatial,
              pads=None,
              ):
    out_shape = [0] * len(input_spatial_shape)
    if auto_pad in ('SAME_UPPER', 'SAME_LOWER'):
        for i in range(len(input_spatial_shape)):
            out_shape[i] = int(
                np.ceil(
                    float(input_spatial_shape[i]) / float(strides_spatial[i])))
    elif auto_pad == 'VALID':
        for i in range(len(input_spatial_shape)):
            out_shape[i] = int((input_spatial_shape[i] - kernel_spatial_shape[i] - (kernel_spatial_shape[i] - 1) * (
                        dilations[i] - 1)) / float(strides_spatial[i]) + 1)
    return out_shape


def Conv(inputs, outputs, attr=None, op_version=11):  # type: (np.ndarray) -> np.ndarray
    from .max_pool import get_pad_shape
    x = inputs[0]
    spatial_rank = x.ndim - 2
    x_shape = np.shape(x)
    w = inputs[1]

    auto_pad = attr.get('auto_pad', 'NOTSET')
    kernel_shape = attr.get('kernel_shape', w.shape[2:])
    b = np.zeros([w.shape[0]], dtype=np.float32) if len(inputs) == 2 else inputs[2]
    dilations = attr.get('dilations', [1] * spatial_rank)
    groups = attr.get('group',1 )
    pads = attr.get('pads', [0, 0]*spatial_rank)
    strides = attr.get('strides', [1]*spatial_rank)

    padding_model = 'VALID'
    is_dilation = False
    for d in dilations:
        if d > 1:
            is_dilation = True
            padding_model = 'SAME'
            break

    if auto_pad != 'NOTSET':
        out_spatial_shape = get_output_shape(auto_pad, kernel_shape, x_shape[2:], dilations, strides, pads)
        pads = get_pad_shape(auto_pad, x_shape[2:], kernel_shape, strides_spatial=strides, dilations_spatial=dilations,
                             output_spatial_shape=out_spatial_shape, spatial_pad=pads)
        pads = [(0, 0), (0, 0)] + pads
    elif auto_pad == 'NOTSET':
        pads = [(0,0), (0,0)] + [(pads[i], pads[i+spatial_rank]) for i in range(spatial_rank)]

    if is_dilation == False:
        x = np.pad(x, pads, mode='constant', constant_values=0)
    perm = [0] + [i + 2 for i in range(spatial_rank)] + [1]
    x = np.transpose(x, perm)
    perm = [i + 2 for i in range(spatial_rank)] + [1, 0]
    if groups == x_shape[1] and spatial_rank == 2 and groups > 1:
        w = np.transpose(w, [2, 3, 0, 1])
    else:
        w = np.transpose(w, perm)

    tf.enable_eager_execution()
    if groups == x_shape[1] and spatial_rank == 2 and groups > 1:
        conv = tf.nn.depthwise_conv2d_native(x, w, strides=[1]+strides+[1], dilations=[1]+dilations+[1], padding=padding_model)
    elif groups != 1:
        input_groups = tf.split(axis=-1, value=x, num_or_size_splits=groups)
        kernel_groups = tf.split(axis=-1, value=w, num_or_size_splits=groups)
        output_groups = list()
        for i, k in zip(input_groups, kernel_groups):
            child = tf.nn.convolution(i, k, strides=strides, dilation_rate=dilations, padding=padding_model)
            output_groups.append(child)
        conv = tf.concat(axis=-1, values=output_groups)
    else:
        conv = tf.nn.convolution(x, w, strides=strides, dilation_rate=dilations, padding=padding_model)
    perm = [0] + [spatial_rank + 1] + [i + 1 for i in range(spatial_rank)]
    conv = tf.nn.bias_add(conv, b)
    res = tf.transpose(conv, perm)
    return res.numpy()
