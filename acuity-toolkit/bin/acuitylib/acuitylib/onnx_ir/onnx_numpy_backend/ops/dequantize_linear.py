from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import numpy as np  # type: ignore


def DequantizeLinear(inputs, outputs, attr=None, op_version=11):  # type: (np.ndarray) -> np.ndarray
    x_zero_point = np.zeros_like(inputs[1], dtype=inputs[0].dtype) if len(inputs) == 2 else inputs[3]
    x = inputs[0]
    x_scale = inputs[1]
    res = (x - x_zero_point) * x_scale
    return res
