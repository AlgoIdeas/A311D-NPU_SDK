# -*- mode: python -*-
import platform
from PyInstaller.utils.hooks import collect_submodules, collect_data_files

# CAUTION*** use root account, PyInstaller 3.4, python 3.5 to pack acuity binary on ubuntu 16.04
# CAUTION*** use root account, PyInstaller 3.4, python 3.6 to pack acuity binary on ubuntu 18.04

# add hidden imports
h5py_hidden_import = collect_submodules('h5py')
tf_1_x_hidden_import = []
tf_2_0_0_hidden_import = collect_submodules('tensorflow_core')
tf_hidden_import = tf_2_0_0_hidden_import
hidden_imports = h5py_hidden_import + tf_hidden_import

# add datas
tf_1_x_datas = []
tf_2_0_0_datas = collect_data_files('tensorflow_core', subdir=None, include_py_files=True)
tf_datas = tf_2_0_0_datas
additional_datas = tf_datas

#add binaries for tensorflow.contrib, onnx
python_version = platform.python_version().split('.')
machine_python_ver_major = python_version[0]
machine_python_ver_minor = python_version[1]
python_folder_name = "{}.{}".format(machine_python_ver_major,machine_python_ver_minor)
python_folder_name_squeeze_dot = "{}{}".format(machine_python_ver_major,machine_python_ver_minor)
third_binaries_tf_1_10_0_cpu=[
("/usr/local/lib/python{}/dist-packages/tensorflow/contrib/nccl/python/ops/_nccl_ops.so".format(python_folder_name), './tensorflow/contrib/nccl/python/ops/'),
("/usr/local/lib/python{}/dist-packages/tensorflow/contrib/tpu/python/ops/_tpu_ops.so".format(python_folder_name), './tensorflow/contrib/tpu/python/ops/'),
("/usr/local/lib/python{}/dist-packages/tensorflow/contrib/text/python/ops/_skip_gram_ops.so".format(python_folder_name), './tensorflow/contrib/text/python/ops/'),
("/usr/local/lib/python{}/dist-packages/tensorflow/contrib/data/_dataset_ops.so".format(python_folder_name), './tensorflow/contrib/data/'),
("/usr/local/lib/python{}/dist-packages/tensorflow/contrib/data/python/ops/__init__.py".format(python_folder_name), './tensorflow/contrib/data/python/ops/'),
("/usr/local/lib/python{}/dist-packages/tensorflow/contrib/periodic_resample/python/ops/_periodic_resample_op.so".format(python_folder_name), './tensorflow/contrib/periodic_resample/python/ops/'),
("/usr/local/lib/python{}/dist-packages/tensorflow/contrib/fused_conv/python/ops/_fused_conv2d_bias_activation_op.so".format(python_folder_name), './tensorflow/contrib/fused_conv/python/ops/'),
("/usr/local/lib/python{}/dist-packages/tensorflow/contrib/tensor_forest/python/ops/_tensor_forest_ops.so".format(python_folder_name), './tensorflow/contrib/tensor_forest/python/ops/'),
("/usr/local/lib/python{}/dist-packages/tensorflow/contrib/tensor_forest/python/ops/_stats_ops.so".format(python_folder_name), './tensorflow/contrib/tensor_forest/python/ops/'),
("/usr/local/lib/python{}/dist-packages/tensorflow/contrib/tensor_forest/python/ops/_model_ops.so".format(python_folder_name), './tensorflow/contrib/tensor_forest/python/ops/'),
("/usr/local/lib/python{}/dist-packages/tensorflow/contrib/tensor_forest/libforestprotos.so".format(python_folder_name), './tensorflow/contrib/tensor_forest/'),
("/usr/local/lib/python{}/dist-packages/tensorflow/contrib/tensor_forest/hybrid/python/ops/_training_ops.so".format(python_folder_name), './tensorflow/contrib/tensor_forest/hybrid/python/ops/'),
("/usr/local/lib/python{}/dist-packages/tensorflow/contrib/rnn/python/ops/_gru_ops.so".format(python_folder_name), './tensorflow/contrib/rnn/python/ops/'),
("/usr/local/lib/python{}/dist-packages/tensorflow/contrib/rnn/python/ops/_lstm_ops.so".format(python_folder_name), './tensorflow/contrib/rnn/python/ops/'),
("/usr/local/lib/python{}/dist-packages/tensorflow/contrib/libsvm/python/ops/_libsvm_ops.so".format(python_folder_name), './tensorflow/contrib/libsvm/python/ops/'),
("/usr/local/lib/python{}/dist-packages/tensorflow/contrib/lite/python/interpreter_wrapper/_tensorflow_wrap_interpreter_wrapper.so".format(python_folder_name), './tensorflow/contrib/lite/python/interpreter_wrapper/'),
("/usr/local/lib/python{}/dist-packages/tensorflow/contrib/lite/toco/python/_tensorflow_wrap_toco.so".format(python_folder_name), './tensorflow/contrib/lite/toco/python/'),
("/usr/local/lib/python{}/dist-packages/tensorflow/contrib/factorization/python/ops/_factorization_ops.so".format(python_folder_name), './tensorflow/contrib/factorization/python/ops/'),
("/usr/local/lib/python{}/dist-packages/tensorflow/contrib/factorization/python/ops/_clustering_ops.so".format(python_folder_name), './tensorflow/contrib/factorization/python/ops/'),
("/usr/local/lib/python{}/dist-packages/tensorflow/contrib/layers/python/ops/_sparse_feature_cross_op.so".format(python_folder_name), './tensorflow/contrib/layers/python/ops/'),
("/usr/local/lib/python{}/dist-packages/tensorflow/contrib/input_pipeline/python/ops/_input_pipeline_ops.so".format(python_folder_name), './tensorflow/contrib/input_pipeline/python/ops/'),
("/usr/local/lib/python{}/dist-packages/tensorflow/contrib/resampler/python/ops/_resampler_ops.so".format(python_folder_name), './tensorflow/contrib/resampler/python/ops/'),
("/usr/local/lib/python{}/dist-packages/tensorflow/contrib/memory_stats/python/ops/_memory_stats_ops.so".format(python_folder_name), './tensorflow/contrib/memory_stats/python/ops/'),
("/usr/local/lib/python{}/dist-packages/tensorflow/contrib/proto/python/kernel_tests/libtestexample.so".format(python_folder_name), './tensorflow/contrib/proto/python/kernel_tests/'),
("/usr/local/lib/python{}/dist-packages/tensorflow/contrib/ffmpeg/ffmpeg.so".format(python_folder_name), './tensorflow/contrib/ffmpeg/'),
("/usr/local/lib/python{}/dist-packages/tensorflow/contrib/rpc/python/kernel_tests/libtestexample.so".format(python_folder_name), './tensorflow/contrib/rpc/python/kernel_tests/'),
("/usr/local/lib/python{}/dist-packages/tensorflow/contrib/reduce_slice_ops/python/ops/_reduce_slice_ops.so".format(python_folder_name), './tensorflow/contrib/reduce_slice_ops/python/ops/'),
("/usr/local/lib/python{}/dist-packages/tensorflow/contrib/boosted_trees/python/ops/_boosted_trees_ops.so".format(python_folder_name), './tensorflow/contrib/boosted_trees/python/ops/'),
("/usr/local/lib/python{}/dist-packages/tensorflow/contrib/image/python/ops/_single_image_random_dot_stereograms.so".format(python_folder_name), './tensorflow/contrib/image/python/ops/'),
("/usr/local/lib/python{}/dist-packages/tensorflow/contrib/image/python/ops/_image_ops.so".format(python_folder_name), './tensorflow/contrib/image/python/ops/'),
("/usr/local/lib/python{}/dist-packages/tensorflow/contrib/image/python/ops/_distort_image_ops.so".format(python_folder_name), './tensorflow/contrib/image/python/ops/'),
("/usr/local/lib/python{}/dist-packages/tensorflow/contrib/tensorrt/python/ops/_trt_engine_op.so".format(python_folder_name), './tensorflow/contrib/tensorrt/python/ops/'),
("/usr/local/lib/python{}/dist-packages/tensorflow/contrib/tensorrt/_wrap_conversion.so".format(python_folder_name), './tensorflow/contrib/tensorrt/'),
("/usr/local/lib/python{}/dist-packages/tensorflow/contrib/kafka/_dataset_ops.so".format(python_folder_name), './tensorflow/contrib/kafka/'),
("/usr/local/lib/python{}/dist-packages/tensorflow/contrib/framework/python/ops/_variable_ops.so".format(python_folder_name), './tensorflow/contrib/framework/python/ops/'),
("/usr/local/lib/python{}/dist-packages/tensorflow/contrib/nearest_neighbor/python/ops/_nearest_neighbor_ops.so".format(python_folder_name), './tensorflow/contrib/nearest_neighbor/python/ops/'),
("/usr/local/lib/python{}/dist-packages/tensorflow/contrib/kinesis/_dataset_ops.so".format(python_folder_name), './tensorflow/contrib/kinesis/'),
("/usr/local/lib/python{}/dist-packages/tensorflow/contrib/coder/python/ops/_coder_ops.so".format(python_folder_name), './tensorflow/contrib/coder/python/ops/'),
("/usr/local/lib/python{}/dist-packages/tensorflow/contrib/seq2seq/python/ops/_beam_search_ops.so".format(python_folder_name), './tensorflow/contrib/seq2seq/python/ops/'),
("/usr/local/lib/python{}/dist-packages/tensorflow/contrib/bigtable/python/ops/_bigtable.so".format(python_folder_name), './tensorflow/contrib/bigtable/python/ops/')]

third_binaries_tf_1_13_2_cpu=[
("/usr/local/lib/python{}/dist-packages/tensorflow/contrib/periodic_resample/python/ops/_periodic_resample_op.so".format(python_folder_name), './tensorflow/contrib/periodic_resample/python/ops/'),
("/usr/local/lib/python{}/dist-packages/tensorflow/contrib/resampler/python/ops/_resampler_ops.so".format(python_folder_name), './tensorflow/contrib/resampler/python/ops/'),
("/usr/local/lib/python{}/dist-packages/tensorflow/contrib/input_pipeline/python/ops/_input_pipeline_ops.so".format(python_folder_name), './tensorflow/contrib/input_pipeline/python/ops/'),
("/usr/local/lib/python{}/dist-packages/tensorflow/contrib/ffmpeg/ffmpeg.so".format(python_folder_name), './tensorflow/contrib/ffmpeg/'),
("/usr/local/lib/python{}/dist-packages/tensorflow/contrib/hadoop/_dataset_ops.so".format(python_folder_name), './tensorflow/contrib/hadoop/'),
("/usr/local/lib/python{}/dist-packages/tensorflow/contrib/tpu/python/ops/_tpu_ops.so".format(python_folder_name), './tensorflow/contrib/tpu/python/ops/'),
("/usr/local/lib/python{}/dist-packages/tensorflow/contrib/ignite/_ignite_ops.so".format(python_folder_name), './tensorflow/contrib/ignite/'),
("/usr/local/lib/python{}/dist-packages/tensorflow/contrib/image/python/ops/_distort_image_ops.so".format(python_folder_name), './tensorflow/contrib/image/python/ops'),
("/usr/local/lib/python{}/dist-packages/tensorflow/contrib/image/python/ops/_single_image_random_dot_stereograms.so".format(python_folder_name), './tensorflow/contrib/image/python/ops/'),
("/usr/local/lib/python{}/dist-packages/tensorflow/contrib/image/python/ops/_image_ops.so".format(python_folder_name), './tensorflow/contrib/image/python/ops/'),
("/usr/local/lib/python{}/dist-packages/tensorflow/contrib/reduce_slice_ops/python/ops/_reduce_slice_ops.so".format(python_folder_name), './tensorflow/contrib/reduce_slice_ops/python/ops/'),
("/usr/local/lib/python{}/dist-packages/tensorflow/contrib/coder/python/ops/_coder_ops.so".format(python_folder_name), './tensorflow/contrib/coder/python/ops/'),
("/usr/local/lib/python{}/dist-packages/tensorflow/contrib/fused_conv/python/ops/_fused_conv2d_bias_activation_op.so".format(python_folder_name), './tensorflow/contrib/fused_conv/python/ops/'),
("/usr/local/lib/python{}/dist-packages/tensorflow/contrib/bigtable/python/ops/_bigtable.so".format(python_folder_name), './tensorflow/contrib/bigtable/python/ops/'),
("/usr/local/lib/python{}/dist-packages/tensorflow/contrib/tensorrt/_wrap_conversion.so".format(python_folder_name), './tensorflow/contrib/tensorrt/'),
("/usr/local/lib/python{}/dist-packages/tensorflow/contrib/tensorrt/python/ops/_trt_engine_op.so".format(python_folder_name), './tensorflow/contrib/tensorrt/'),
("/usr/local/lib/python{}/dist-packages/tensorflow/contrib/boosted_trees/python/ops/_boosted_trees_ops.so".format(python_folder_name), './tensorflow/contrib/boosted_trees/python/ops/'),
("/usr/local/lib/python{}/dist-packages/tensorflow/contrib/memory_stats/python/ops/_memory_stats_ops.so".format(python_folder_name), './tensorflow/contrib/memory_stats/python/ops/'),
("/usr/local/lib/python{}/dist-packages/tensorflow/contrib/rnn/python/ops/_gru_ops.so".format(python_folder_name), './tensorflow/contrib/rnn/python/ops/'),
("/usr/local/lib/python{}/dist-packages/tensorflow/contrib/rnn/python/ops/_lstm_ops.so".format(python_folder_name), './tensorflow/contrib/rnn/python/ops/'),
("/usr/local/lib/python{}/dist-packages/tensorflow/contrib/nearest_neighbor/python/ops/_nearest_neighbor_ops.so".format(python_folder_name), './tensorflow/contrib/nearest_neighbor/python/ops'),
("/usr/local/lib/python{}/dist-packages/tensorflow/contrib/framework/python/ops/_variable_ops.so".format(python_folder_name), './tensorflow/contrib/framework/python/ops/'),
("/usr/local/lib/python{}/dist-packages/tensorflow/contrib/seq2seq/python/ops/_beam_search_ops.so".format(python_folder_name), './tensorflow/contrib/seq2seq/python/ops/'),
("/usr/local/lib/python{}/dist-packages/tensorflow/contrib/libsvm/python/ops/_libsvm_ops.so".format(python_folder_name), './tensorflow/contrib/libsvm/python/ops/'),
("/usr/local/lib/python{}/dist-packages/tensorflow/contrib/factorization/python/ops/_clustering_ops.so".format(python_folder_name), './tensorflow/contrib/factorization/python/ops/'),
("/usr/local/lib/python{}/dist-packages/tensorflow/contrib/factorization/python/ops/_factorization_ops.so".format(python_folder_name), './tensorflow/contrib/factorization/python/ops/'),
("/usr/local/lib/python{}/dist-packages/tensorflow/contrib/kinesis/_dataset_ops.so".format(python_folder_name), './tensorflow/contrib/kinesis/'),
("/usr/local/lib/python{}/dist-packages/tensorflow/contrib/rpc/python/kernel_tests/libtestexample.so".format(python_folder_name), './tensorflow/contrib/rpc/python/kernel_tests/'),
("/usr/local/lib/python{}/dist-packages/tensorflow/contrib/kafka/_dataset_ops.so".format(python_folder_name), './tensorflow/contrib/kafka/'),
("/usr/local/lib/python{}/dist-packages/tensorflow/contrib/layers/python/ops/_sparse_feature_cross_op.so".format(python_folder_name), './tensorflow/contrib/layers/python/ops/'),
("/usr/local/lib/python{}/dist-packages/tensorflow/contrib/text/python/ops/_skip_gram_ops.so".format(python_folder_name), './tensorflow/contrib/text/python/ops/'),
("/usr/local/lib/python{}/dist-packages/tensorflow/contrib/tensor_forest/hybrid/python/ops/_training_ops.so".format(python_folder_name), './tensorflow/contrib/tensor_forest/hybrid/python/ops/'),
("/usr/local/lib/python{}/dist-packages/tensorflow/contrib/tensor_forest/libforestprotos.so".format(python_folder_name), './tensorflow/contrib/tensor_forest/'),
("/usr/local/lib/python{}/dist-packages/tensorflow/contrib/tensor_forest/python/ops/_model_ops.so".format(python_folder_name), './tensorflow/contrib/tensor_forest/python/ops/'),
("/usr/local/lib/python{}/dist-packages/tensorflow/contrib/tensor_forest/python/ops/_stats_ops.so".format(python_folder_name), './tensorflow/contrib/tensor_forest/python/ops/'),
("/usr/local/lib/python{}/dist-packages/tensorflow/contrib/tensor_forest/python/ops/_tensor_forest_ops.so".format(python_folder_name), './tensorflow/contrib/tensor_forest/python/ops/')]

third_binaries_onnx_1_2_2=[
("/usr/local/lib/python{}/dist-packages/onnx/.libs/libprotobuf-487875fe.so.9.0.1".format(python_folder_name), '.'),
("/usr/local/lib/python{}/dist-packages/onnx/onnx_cpp2py_export.cpython-{}m-x86_64-linux-gnu.so".format(python_folder_name,python_folder_name_squeeze_dot), './onnx/')]

third_binaries_onnx_1_3_0=[
("/usr/local/lib/python{}/dist-packages/onnx/.libs/libprotobuf-024eded0.so.9.0.1".format(python_folder_name), '.'),
("/usr/local/lib/python{}/dist-packages/onnx/onnx_cpp2py_export.cpython-{}m-x86_64-linux-gnu.so".format(python_folder_name,python_folder_name_squeeze_dot), './onnx/')]

third_binaries_onnx_1_4_1=third_binaries_onnx_1_3_0

third_binaries_onnx_1_6_0=third_binaries_onnx_1_2_2

third_binaries = third_binaries_onnx_1_6_0

def default_analyze(script):
    a = Analysis([script],
                 pathex=['.'],
                 datas=additional_datas,
                 win_no_prefer_redirects=False,
                 win_private_assemblies=False,
                 hiddenimports=hidden_imports,
                 binaries=third_binaries,
                 )
    return a

def default_pyz_exe(a, name):
    pyz = PYZ(a.pure, a.zipped_data)
    exe = EXE(pyz,
              a.scripts,
              exclude_binaries=True,
              name=name,
              debug=False,
              strip=False,
              upx=True,
              console=True )
    return pyz,exe

def merge_libs(analyzes):
    ret = list()
    for a in analyzes:
        ret.extend([a.binaries,a.zipfiles,a.datas])
    return ret

convert_caffe = default_analyze('convertcaffe.py')
convert_tf = default_analyze('convertensorflow.py')
#export = default_analyze('tensorconverter.py')
tensorzonex = default_analyze('tensorzonex.py')
prune = default_analyze('tensorreduce.py')
ovx_generator = default_analyze('ovxgenerator.py')
convert_tflite = default_analyze('convertflite.py')
convert_darknet = default_analyze('convertdarknet.py')
convert_onnx = default_analyze('convertonnx.py')
pegasus = default_analyze('pegasus.py')
exportflite = default_analyze('exportflite.py')
convert_pytorch = default_analyze('convertpytorch.py')
convert_keras = default_analyze('convertkeras.py')
#convert_caffe = Analysis(['convertcaffe.py'],
#             pathex=['.'],
#             binaries=[],
#             datas=[],
#             hiddenimports=[],
#             hookspath=[],
#             runtime_hooks=[],
#             excludes=[],
#             win_no_prefer_redirects=False,
#             win_private_assemblies=False,
#             cipher=block_cipher)

# Do merge
MERGE((convert_caffe, 'convertcaffe', 'convertcaffe'),
      (convert_tf, 'convertensorflow', 'convertensorflow'),
#      (export, 'tensorconverter', 'tensorconverter'),
      (tensorzonex, 'tensorzonex', 'tensorzonex'),
      (prune, 'tensorreduce', 'tensorreduce'),
      (ovx_generator, 'ovxgenerator', 'ovxgenerator'),
      (convert_tflite, 'convertflite', 'convertflite'),
      (convert_darknet, 'convertdarknet', 'convertdarknet'),
      (convert_onnx, 'convertonnx', 'convertonnx'),
      (pegasus, 'pegasus', 'pegasus'),
      (exportflite, 'exportflite', 'exportflite'),
      (convert_pytorch, 'convertpytorch', 'convertpytorch'),
      (convert_keras, 'convertkeras', 'convertkeras'),
      )

convert_caffe_pyz,convert_caffe_exe = default_pyz_exe(convert_caffe, 'convertcaffe')
convert_tf_pyz,convert_tf_exe = default_pyz_exe(convert_tf, 'convertensorflow')
#export_pyz,export_exe = default_pyz_exe(export, 'tensorconverter')
tensorzonex_pyz,tensorzonex_exe = default_pyz_exe(tensorzonex, 'tensorzonex')
prune_pyz,prune_exe = default_pyz_exe(prune, 'tensorreduce')
ovx_generator_pyz,ovx_generator_exe = default_pyz_exe(ovx_generator, 'ovxgenerator')
convert_tflite_pyz,convert_tflite_exe = default_pyz_exe(convert_tflite, 'convertflite')
convert_darknet_pyz,convert_darknet_exe = default_pyz_exe(convert_darknet, 'convertdarknet')
convert_onnx_pyz,convert_onnx_exe = default_pyz_exe(convert_onnx, 'convertonnx')
pegasus_pyz,pegasus_exe = default_pyz_exe(pegasus, 'pegasus')
exportflite_pyz,exportflite_exe = default_pyz_exe(exportflite, 'exportflite')
convert_pytorch_pyz,convert_pytorch_exe = default_pyz_exe(convert_pytorch, 'convertpytorch')
convert_keras_pyz,convert_keras_exe = default_pyz_exe(convert_keras, 'convertkeras')

# Collections
libs = merge_libs([
                   convert_caffe,
                   convert_tf,
#                   export,
                   tensorzonex,
                   prune,
                   ovx_generator,
                   convert_tflite,
                   convert_darknet,
                   convert_onnx,
                   pegasus,
                   exportflite,
                   convert_pytorch,
                   convert_keras
                   ])

coll_exe = COLLECT(convert_caffe_exe,
               convert_tf_exe,
#               export_exe,
               tensorzonex_exe,
               prune_exe,
               ovx_generator_exe,
               convert_tflite_exe,
               convert_darknet_exe,
               convert_onnx_exe,
               pegasus_exe,
               exportflite_exe,
               convert_pytorch_exe,
               convert_keras_exe,
               strip=None,
               upx=True,
               name = 'executable'
               )

coll_lib = COLLECT(*libs,
               strip=None,
               upx=True,
               name = 'libs'
               )

