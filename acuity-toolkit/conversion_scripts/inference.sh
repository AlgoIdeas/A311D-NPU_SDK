#!/bin/bash

NAME=mobilenet_tf
ACUITY_PATH=../bin/

convert_caffe=${ACUITY_PATH}convertcaffe
convert_tf=${ACUITY_PATH}convertensorflow
tensorzone=${ACUITY_PATH}tensorzonex
export_ovxlib=${ACUITY_PATH}ovxgenerator

if [ ! -e "$convert_caffe" ]; then
    convert_caffe=${ACUITY_PATH}convertcaffe.py
fi

if [ ! -e "$convert_tf" ]; then
    convert_tf=${ACUITY_PATH}convertensorflow.py
fi

if [ ! -e "$tensorzone" ]; then
    tensorzone=${ACUITY_PATH}tensorzonex.py
fi

if [ ! -e "$tensorconverter" ]; then
    export_ovxlib=${ACUITY_PATH}ovxgenerator.py
fi

$tensorzone \
    --action inference \
    --source text \
    --source-file ./data/validation_tf.txt \
    --channel-mean-value '128 128 128 128' \
    --model-input ${NAME}.json \
    --model-data ${NAME}.data \
    --dtype quantized


