#!/bin/bash

NAME=mobilenet_tf
ACUITY_PATH=../bin/

tensorzone=${ACUITY_PATH}tensorzonex

#dynamic_fixed_point-i8 asymmetric_affine-u8
$tensorzone \
    --action quantization \
    --dtype float32 \
    --source text \
    --source-file ./data/validation_tf.txt \
    --channel-mean-value '128 128 128 128' \
    --model-input ${NAME}.json \
    --model-data ${NAME}.data \
    --model-quantize ${NAME}.quantize \
    --quantized-dtype dynamic_fixed_point-i8 \
    --quantized-rebuild

